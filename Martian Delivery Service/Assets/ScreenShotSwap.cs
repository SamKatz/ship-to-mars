﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShotSwap : MonoBehaviour {
    [SerializeField]
    public List<GameObject> HideObjects;
    [SerializeField]
    public List<GameObject> ShowObjects;

    public void SwapObjects()
    {
        foreach (GameObject g in ShowObjects)
        {
            if (g != null)
            {
                g.SetActive(true);
            }
        }

        foreach (GameObject g in HideObjects)
        {
            if (g != null)
            {
                g.SetActive(false);
            }
        }
    }
}
