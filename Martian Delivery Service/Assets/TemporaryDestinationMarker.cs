﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporaryDestinationMarker : MonoBehaviour {
    public SpaceMovementManager movement;
    public Color color;


    private SpriteRenderer sr;
    public Sprite dead;
    public Sprite damage;
    public Sprite arrow;

    public float zLevel = -0.005f;

	// Use this for initialization
	void Awake () {
        sr = GetComponent<SpriteRenderer>();
        
	}
	
	// Update is called once per frame
	void Update () { //OPTIMIZATION: caching

        Route route = movement.GetRoute();

        List<Int2> waypoints = route.waypoints;
        Int2 location = waypoints[waypoints.Count - 1];
        this.transform.position = HexUtils.HexToTransform(location);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, zLevel);

        HexUtils.Direction direction = route.bearings[route.bearings.Count - 1];

        gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, HexUtils.GetAngle(direction));


        if (sr != null)
        {
            sr.color = color;
            if (movement == null)
            {
                sr.enabled = false;
            }
            else
            {
                sr.enabled = true;
            }

            if (route.isFatal)
            {
                sr.sprite = dead;
                gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, 0);
            }
            else if (route.willDamageShip)
            {
                sr.sprite = damage;
                gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, 0);
            }
            else
            {
                sr.sprite = arrow;
            }
        }
        
    }
}
