﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorAtRuntime : MonoBehaviour {
    public Color col = Color.white;
	// Use this for initialization
	void Start () {
        Utils.SetRenderColor(this.GetComponent<Renderer>(), col);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
