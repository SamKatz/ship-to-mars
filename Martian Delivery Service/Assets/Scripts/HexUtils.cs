﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class HexUtils : MonoBehaviour {
    public static int mapRadius = 24;
    public static int hexSide = 1;

    public enum Direction
    {
        NORTH, NORTHEAST, SOUTHEAST, SOUTH, SOUTHWEST, NORTHWEST
    }



    public static bool IsInMap(Int2 hexCoords)
    {
        return Mathf.Abs(hexCoords.x) < mapRadius &&
               Mathf.Abs(hexCoords.y) < mapRadius &&
               Mathf.Abs(hexCoords.x + hexCoords.y) < mapRadius;
    }

    public static float GetAngle(Direction dir)
    {
        switch (dir)
        {
            case Direction.SOUTHEAST:
                return 330;
            case Direction.SOUTH:
                return 270;
            case Direction.SOUTHWEST:
                return 210;
            case Direction.NORTHWEST:
                return 150;
            case Direction.NORTH:
                return 90;
            case Direction.NORTHEAST:
                return 30;

        }

        throw new System.ArgumentException("Invalid Direction");
    }

    public static Int2 ToInt2(Vector2 vec2)
    {
        return new Int2((int)vec2.x, (int)vec2.y);
    }

    public static Vector3 ToVec3(Vector2 vec2)
    {
        return new Vector3(vec2.x, vec2.y, 0);
    }

    public static Vector3 ToVec2(Vector3 vec3)
    {
        return new Vector2(vec3.x, vec3.y);
    }

    public static Vector2 GetVector(Direction dir)
    {
        float angle = GetAngle(dir);
        return new Vector2(Mathf.Cos(Mathf.Deg2Rad * angle), Mathf.Sin(Mathf.Deg2Rad * angle));
    }

    public static Int2 GetCoordinateVector(Direction dir)
    {
        switch (dir)
        {
            case Direction.NORTH:
                return new Int2(0, 1);
            case Direction.NORTHEAST:
                return new Int2(1, 0);
            case Direction.SOUTHEAST:
                return new Int2(1, -1);
            case Direction.SOUTH:
                return new Int2(0, -1);
            case Direction.SOUTHWEST:
                return new Int2(-1, 0);
            case Direction.NORTHWEST:
                return new Int2(-1, 1);
        }

        throw new System.ArgumentException("Invalid Direction");
    }

    public static Direction NextClockwise(Direction dir)
    {
        switch (dir)
        {
            case Direction.NORTH:
                return Direction.NORTHEAST;
            case Direction.NORTHEAST:
                return Direction.SOUTHEAST;
            case Direction.SOUTHEAST:
                return Direction.SOUTH;
            case Direction.SOUTH:
                return Direction.SOUTHWEST;
            case Direction.SOUTHWEST:
                return Direction.NORTHWEST;
            case Direction.NORTHWEST:
                return Direction.NORTH;
        }
        throw new System.ArgumentException("Invalid Direction");
    }

    public static Direction NextCounterclockwise(Direction dir)
    {
        switch (dir)
        {
            case Direction.NORTH:
                return Direction.NORTHWEST;
            case Direction.NORTHEAST:
                return Direction.NORTH;
            case Direction.SOUTHEAST:
                return Direction.NORTHEAST;
            case Direction.SOUTH:
                return Direction.SOUTHEAST;
            case Direction.SOUTHWEST:
                return Direction.SOUTH;
            case Direction.NORTHWEST:
                return Direction.SOUTHWEST;
        }
        throw new System.ArgumentException("Invalid Direction");
    }

    public static bool IsTurnAllowed(Int2 shipLocation, Direction oldDirection, Direction newDirection)
    {
        //vector from ship to sun
        Vector2 shipToSun = HexUtils.ToVec2(HexUtils.HexToTransform(new Int2(0, 0))) - HexUtils.ToVec2(HexUtils.HexToTransform(shipLocation));
        //vector for current ship direction
        Vector2 shipDir = HexUtils.GetVector(oldDirection);
        //vector for new ship direction
        Vector2 newShipDir = HexUtils.GetVector(newDirection);


        float shipDirAngle = Vector2.SignedAngle(shipToSun, shipDir);
        float newDirAngle = Vector2.SignedAngle(shipToSun, newShipDir);

        if (Math.Abs(newDirAngle) < 1) return false;

        if (Math.Sign(shipDirAngle) == -1 * Math.Sign(newDirAngle)) return false;
        return true;

    }



    public static Vector3 HexToTransform(Int2 location)
    {

        return new Vector3(hexSide * 3f / 2 * location.x,
                           hexSide * Mathf.Sqrt(3) * ((float)location.y + ((float)location.x / 2f)),
                           0);

    }

    public static bool IsOnTurnLine(Int2 loc)
    {
        return loc.x == 0 || loc.y == 0 || (Math.Abs(loc.x) == Math.Abs(loc.y) && Math.Sign(loc.x) != Math.Sign(loc.y));
    }


    
    public static void SetViewAll(List<Int2> hexthings, Camera cam, float padAmount)
    {
        float maxX = float.MinValue;
        float minX = float.MaxValue;
        float maxY = float.MinValue;
        float minY = float.MaxValue;

        foreach(Int2 hex in hexthings)
        {
            Vector3 loc = HexUtils.HexToTransform(hex);

            maxX = Mathf.Max(loc.x, maxX);
            minX = Mathf.Min(loc.x, minX);

            maxY = Mathf.Max(loc.y, maxY);
            minY = Mathf.Min(loc.y, minY);
        }

        maxX += padAmount;
        minX -= padAmount;
        maxY += padAmount;
        minY -= padAmount;

        Vector2 center = new Vector2((maxX + minX)/2, (minY + maxY) / 2);

        cam.transform.position = new Vector3((maxX + minX) / 2, (minY + maxY) / 2, cam.transform.position.z);



        float requiredVerticalOrthSize = Mathf.Max(Mathf.Abs(center.y - maxY), Mathf.Abs(center.y - minY));
        float requiredHorizontalOrthSize = Mathf.Max(Mathf.Abs(center.x - maxX), Mathf.Abs(center.x - minX));

        cam.orthographicSize = Mathf.Max(requiredVerticalOrthSize, requiredHorizontalOrthSize / cam.aspect );



    }

    public static void SetViewAll3D(List<Int2> hexthings, Camera cam, float extraCameraPadding)
    {
        float maxX = float.MinValue;
        float minX = float.MaxValue;
        float maxY = float.MinValue;
        float minY = float.MaxValue;

        foreach (Int2 hex in hexthings)
        {
            Vector3 loc = HexUtils.HexToTransform(hex);

            maxX = Mathf.Max(loc.x, maxX);
            minX = Mathf.Min(loc.x, minX);

            maxY = Mathf.Max(loc.y, maxY);
            minY = Mathf.Min(loc.y, minY);
        }

        maxX += extraCameraPadding;
        minX -= extraCameraPadding;
        maxY += extraCameraPadding;
        minY -= extraCameraPadding;

        Vector2 center = new Vector2((maxX + minX) / 2, (minY + maxY) / 2);

        cam.transform.position = new Vector3((maxX + minX) / 2, (minY + maxY) / 2, cam.transform.position.z);



        float requiredVerticalOrthSize = Mathf.Max(Mathf.Abs(center.y - maxY), Mathf.Abs(center.y - minY));
        float requiredHorizontalOrthSize = Mathf.Max(Mathf.Abs(center.x - maxX), Mathf.Abs(center.x - minX));

        cam.orthographicSize = Mathf.Max(requiredVerticalOrthSize, requiredHorizontalOrthSize / cam.aspect);



    }

    public static int[] HexToCubic(Int2 hex)
    {
        return new int[] {hex.x, hex.y, -hex.x-hex.y};
    }

    public static int HexDistance(Int2 hex1, Int2 hex2) //derived from converting to cubic coordinates first, then going doing cube distance. Drawback of this coordinate system.
    {
        int[] cube1 = HexToCubic(hex1);
        int[] cube2 = HexToCubic(hex2);

        return (Math.Abs(cube1[0] - cube2[0]) + Math.Abs(cube1[1] - cube2[1]) + Math.Abs(cube1[2] - cube2[2])) / 2;

    }

}
