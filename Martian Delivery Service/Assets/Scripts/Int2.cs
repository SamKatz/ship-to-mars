﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public class Int2 {
    [SerializeField]
    public int x;
    [SerializeField]
    public int y;

    public Int2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static Int2 operator+ (Int2 v1, Int2 v2)
    {
        return new Int2(v1.x + v2.x, v1.y + v2.y);
    }

    public static Int2 operator- (Int2 v1, Int2 v2)
    {
        return new Int2(v1.x - v2.x, v1.y - v2.y);
    }

    public static Int2 operator*(Int2 v1, int scalar)
    {
        return new Int2(v1.x * scalar, v1.y * scalar);
    }

    public static Int2 operator *(int scalar, Int2 v1)
    {
        return v1 * scalar;
    }

    public Vector2 toVector2()
    {
        return new Vector2(x, y);
    }



}
