﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoKillSwitch : MonoBehaviour {

    float lastInteracted;
    public float timeOutDuration = 120;

    bool killswitch = true;

	// Use this for initialization
	void Start () {
        lastInteracted = Time.time;
	}

    private void OnGUI()
    {
        if(!killswitch) GUI.Label(new Rect(300, 10, 400, 20), "Auto reset off.");
    }

    // Update is called once per frame
    void Update () {
        if (Input.anyKeyDown)
        {
            lastInteracted = Time.time;
        }

        if(Time.time - timeOutDuration > lastInteracted && killswitch)
        {

            for(int i = 0; i < Game.numPlayers; i++) Game.RecordString("WALKAWAY");

            Utils.Restart();
        }

        if (Input.GetKeyDown(KeyCode.F10))
        {
            killswitch = !killswitch;
        }



	}
}
