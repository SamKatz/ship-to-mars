﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route{
    public readonly List<Int2> waypoints;
    public readonly List<HexUtils.Direction> bearings;
    public readonly bool isFatal;
    public readonly bool entersSun; //subset of isFatal
    public readonly bool exitsSolarSystem; //subset of isFatal



    public readonly bool willDamageShip; //mutually exclusive with isFatal
    public readonly bool manueverDamage;
    public readonly bool orbitDamage;
    public readonly bool sunDamage;


    public Route(List<Int2> waypoints, List<HexUtils.Direction> bearings, bool entersSun, bool exitsSolarSystem, bool manueverDamage, bool orbitDamage, bool sunDamage)
    {
        this.waypoints = waypoints;
        this.bearings = bearings;
        this.isFatal = entersSun || exitsSolarSystem;

        this.entersSun = entersSun;
        this.exitsSolarSystem = exitsSolarSystem;

        this.willDamageShip = manueverDamage || orbitDamage || sunDamage;

        this.manueverDamage = manueverDamage;
        this.orbitDamage = orbitDamage;
        this.sunDamage = sunDamage;
    }
}
