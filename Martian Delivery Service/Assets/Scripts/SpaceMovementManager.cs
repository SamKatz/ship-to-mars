﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceMovementManager : MonoBehaviour {
    private static List<SpaceMovementManager> spaceObjects = new List<SpaceMovementManager>();
    private static List<SpaceMovementManager> planets = new List<SpaceMovementManager>();

    //many fields are public to allow easy access from the Unity editor

    public Int2 location;
    public HexUtils.Direction travelDir = HexUtils.Direction.NORTH;
    public HexUtils.Direction displayDir = HexUtils.Direction.NORTH;

    public Card lastCard;

    public int speed = 2;

    public bool clockwise
    {
        get {
            Vector3 sunToShip = HexUtils.HexToTransform(location) - HexUtils.HexToTransform(new Int2(0, 0));
            Vector3 sunToDest = HexUtils.HexToTransform(location + HexUtils.GetCoordinateVector(travelDir)) - HexUtils.HexToTransform(new Int2(0, 0));
            
            return Vector3.SignedAngle(sunToShip, sunToDest, new Vector3(0,0,1)) < 0;
        }
    }
    public bool isPlanet = false;
    public bool isPredictor = false;
    public bool alwaysStartAnimation = false;


    private bool animating = false;
    private Nullable<float> animationStartTime = null;
    private bool repeat = true;



    private const int defaultDepth = 0;
    private const int activeDepth = -1;

    [SerializeField]
    bool dead = false;

    public SpaceMovementManager predictionOwner = null;
    public SpaceMovementManager orbitingPlanet;

    public Vector3 offsetOnSameSpace;

    public void NextTurn()
    {
        if (orbitingPlanet != null)
        {
            //this.clockwise = orbitingPlanet.clockwise;
            this.travelDir = orbitingPlanet.travelDir;
            this.speed = orbitingPlanet.speed;
            this.location = orbitingPlanet.location;

        } else
        {
            Route route = GetRoute();
            location = route.waypoints[route.waypoints.Count - 1];
            travelDir = route.bearings[route.bearings.Count - 1];
            displayDir = travelDir;
        }
    }

    void Start()
    {
        spaceObjects.Add(this);

        if (this.isPlanet)
        {
            planets.Add(this);
            
        }

        
        displayDir = travelDir;

    }

    public void Kill()
    {
        Destroy(this.gameObject);
        dead = true;
        //Game.GetInstance().CheckIfEndGame();
    }

    public static void CleanSpaceObjectLists()
    {
        for (int i = 0; i < planets.Count; i++)
        {
            if (planets[i] == null)
            {
                planets.RemoveAt(i);
                i--;
                continue;
            }
        }

        for (int i = 0; i < spaceObjects.Count; i++)
        {
            if (spaceObjects[i] == null)
            {
                spaceObjects.RemoveAt(i);
                i--;
                continue;
            }
        }
    }

    public static SpaceMovementManager[] GetPlanets()
    {
        return planets.ToArray();
    }

    public static SpaceMovementManager GetPlanet(Int2 location)
    {
        foreach(SpaceMovementManager planet in planets)
        {
            if(location.x == planet.location.x && location.y == planet.location.y)
            {
                return planet;
            }
        }
        return null;
    }


    public bool IsDead()
    {
        return dead;
    }

    public float zLevel = -10;
    void Update()
    {

        if (isPredictor && predictionOwner != null)
        {
            this.location = predictionOwner.location;
            this.speed = predictionOwner.speed;
            this.travelDir = predictionOwner.travelDir;
            //this.clockwise = predictionOwner.clockwise;
            this.displayDir = predictionOwner.displayDir;
        }


        if (this.alwaysStartAnimation && !animating)
        {
            StartAnimation(true);
        }
        if (this.isPredictor)
        {

            if (!animating)
            {
                StartAnimation(true);
            }


            //TODO: Add animations for orbiting a planet.
            if (Game.GetInstance().GetCurrentPlayer() != null && CardManager.GetInstance().GetActiveCard() != null)
            {
                Utils.EnableAllRenderers(this.gameObject);
            }
            else
            {
                Utils.DisableAllRenderers(this.gameObject);
                StopAnimation();
            }


        }



        //OPTIMIZATION: This code is going to put a fair amount of load on the GC.
        Vector3 hexLoc = HexUtils.HexToTransform(location);

        float z = zLevel;

        //deprecated 2d code
        /*if(Game.GetInstance().GetCurrentPlayer() != null && this == Game.GetInstance().GetCurrentPlayer().ship)
        {
            z = zLevel-2;
        }*/

        try
        {
            if (animating)
            {
                //adjust animation duration based on whether the end cinematic is playing
                float animationDuration =
                    Game.GetInstance().GetCurrentAnimationState() == Game.AnimationState.EndCinematic
                        ? Game.GetInstance().endCinematicLength
                        : Game.GetInstance().animationLength;

                if (animationStartTime + animationDuration <= Time.time)
                {
                    if (!repeat)
                    {
                        StopAnimation();
                        Update();
                        return;
                    }
                    else
                    {
                        StartAnimation(true);
                        Update();
                        return;
                    }
                }

                if (animationStartTime == null) return;
                //determine what two locations/rotations to lerp between

                Route route = this.GetRoute();
                float timePerHex = animationDuration / (route.waypoints.Count - 1);


                int startHexNo = (int) Mathf.Floor((Time.time - (float) animationStartTime) / timePerHex);


                int endHexNo = startHexNo + 1;

                if (Game.GetInstance().GetCurrentAnimationState() == Game.AnimationState.EndCinematic)
                    endHexNo = startHexNo;

                float totalProgress;
                
                totalProgress = (Time.time - (float) animationStartTime) / animationDuration;


                float progressBetweenHexes =
                    ((Time.time - (float) animationStartTime) - (startHexNo * timePerHex)) / timePerHex; //percentage

                Vector3 pos1 = HexUtils.HexToTransform(route.waypoints[startHexNo]);

                if (endHexNo >= route.waypoints.Count)
                {
                    int x = 3;
                }

                Vector3 pos2 = HexUtils.HexToTransform(route.waypoints[endHexNo]);
                //don't move ships to new hexes during end cinematics
                

                float angle1 = HexUtils.GetAngle(route.bearings[startHexNo]);
                if (startHexNo == 0) angle1 = HexUtils.GetAngle(displayDir);

                float angle2 = HexUtils.GetAngle(route.bearings[endHexNo]);

                float currentAngle = Mathf.LerpAngle(angle1, angle2, progressBetweenHexes);
                Vector3 currentPos = Vector3.Lerp(pos1, pos2, progressBetweenHexes);
                currentPos = new Vector3(currentPos.x, currentPos.y, z);

                if (this.orbitingPlanet == null)
                {
                    if (!this.isPlanet && !IsPlanetPredictor())
                    {
                        transform.eulerAngles = new Vector3(0, 0, currentAngle);
                        transform.position = currentPos;

                        Vector3 offset = Vector3.zero;

                        

                    }
                    else
                    {
                        transform.position = currentPos;
                    }
                }
                else
                {

                    float angle = Mathf.Lerp(0, 360 * Game.GetInstance().orbitsPerRound, totalProgress);
                    transform.eulerAngles = new Vector3(0, 0, angle + 90);
                    transform.position = currentPos + 0.5f * HexUtils.hexSide *
                                         new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle), Mathf.Sin(Mathf.Deg2Rad * angle),
                                             0);
                }

                if (isPlanet || isPredictor
                ) //planet predictors can't be planets, or players will be able to land on them
                {
                    foreach (PlanetRotator pr in this.GetComponentsInChildren<PlanetRotator>())
                    {
                        pr.UpdateProgress(totalProgress);
                    }

                }

            }
            else
            {
                gameObject.transform.position = new Vector3(hexLoc.x, hexLoc.y, z);

                if(!isPlanet && !this.IsPlanetPredictor()) gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, HexUtils.GetAngle(displayDir));
                


                if (isPlanet)
                {
                    foreach (PlanetRotator pr in this.GetComponentsInChildren<PlanetRotator>())
                    {
                        pr.UpdateProgress(0);
                    }

                }

            }

            if (!isPlanet && !isPredictor)
            {
                foreach (SpaceMovementManager ship in spaceObjects)
                {
                    if (!ship.isPlanet && !ship.isPredictor && ship.location.x == location.x && ship.location.y == location.y && this != ship)
                    {
                        transform.position += offsetOnSameSpace;
                    }
                }
            }

        }
        catch (Exception e)
        {
            //Debug.LogError("Exception occurred in animation cycle for SpaceMovementManager " + this.name + " with error " + e.StackTrace);
        }


        


        

        /* debug code disabled
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if(HexUtils.IsTurnAllowed(location, travelDir, HexUtils.NextCounterclockwise(travelDir)))
            {
                travelDir = HexUtils.NextCounterclockwise(travelDir);
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (HexUtils.IsTurnAllowed(location, travelDir, HexUtils.NextClockwise(travelDir)))
            {
                travelDir = HexUtils.NextClockwise(travelDir);
            }
        }*/


    }

    public void StartAnimation(bool repeat)
    {
        //Debug.Log("Starting animation for object " + this.name + " from call stack " + Environment.StackTrace);

        this.repeat = repeat;
        animating = true;
        animationStartTime = Time.time;
    }

    public void StopAnimation()
    {
        animating = false;
        animationStartTime = null;
    }

    public static void StopAllAnimations()
    {
        foreach (SpaceMovementManager spaceThing in spaceObjects)
        {
            spaceThing.StopAnimation();
        }
    }

    public bool IsPlanetPredictor()
    {
        return predictionOwner != null && predictionOwner.isPlanet;
    }

    public List<Int2> GetWaypoints()
    {
        return GetRoute().waypoints;
    }

    public Route GetRoute()
    {
        return GetRoute(false);
    }

    public Route GetRoute(bool cardDamage)
    {
        Int2 loc = location;
        HexUtils.Direction currentDir = travelDir;
        List<Int2> waypoints = new List<Int2>();
        List<HexUtils.Direction> directions = new List<HexUtils.Direction>();
        int speedLeft = speed;
        waypoints.Add(loc);
        directions.Add(currentDir);

        bool sunDeath = false;
        bool exitGameDeath = false;

        bool manueverDamage = false;
        bool orbitDamage = false;
        bool sunDamage = false;

        

        while(speedLeft > 0)
        {
            Int2 dir = HexUtils.GetCoordinateVector(currentDir);
            
            
            loc += dir;

            speedLeft--;
            if (HexUtils.IsOnTurnLine(loc))
            {
                if (clockwise)
                {
                    currentDir = HexUtils.NextClockwise(currentDir);
                }
                else
                {
                    currentDir = HexUtils.NextCounterclockwise(currentDir);
                }
                //check if would fly into the sun
                int distance = Math.Max(Math.Abs(loc.x), Math.Abs(loc.y));

                Int2 fallDir = distance * HexUtils.GetCoordinateVector(currentDir);
                Int2 travelTowardsSun = loc + fallDir;
                if (travelTowardsSun.x == 0 && travelTowardsSun.y == 0)
                {
                    //we are flying towards the sun. Move one tick towards it, then reverse the turn.

                    loc += HexUtils.GetCoordinateVector(currentDir);
                    if (!clockwise)
                    {
                        currentDir = HexUtils.NextClockwise(currentDir);
                    }
                    else
                    {
                        currentDir = HexUtils.NextCounterclockwise(currentDir);
                    }
                }
            }

            waypoints.Add(loc);
            directions.Add(currentDir);


            if (!HexUtils.IsInMap(loc))
            {
                exitGameDeath = true;
                break;
            }

            if (loc.x == 0 && loc.y == 0)
            {
                sunDeath = true;
                break;
            }else if (Math.Max(Math.Abs(loc.x), Math.Abs(loc.y)) <= 2) //too close to the sun
            {
                sunDamage = true;
            }
        }

        //Assumption - orbit action does not occur while within range of Sun damage
        if(!sunDeath && !exitGameDeath && cardDamage)
        {
            if (lastCard is OrbitCard)
            {
                orbitDamage = true;
            }
            else if (lastCard != null)
            {
                manueverDamage = true;
            }
        }


        return new Route(waypoints, directions, sunDeath, exitGameDeath, manueverDamage, orbitDamage, sunDamage);
    }

    public void ResetPositionToEarth()
    {
        SpaceMovementManager earth = GameObject.Find("Earth").GetComponent<SpaceMovementManager>();
        Int2 startPos = earth.location;
        travelDir = earth.travelDir;
        location = earth.location;
        //clockwise = earth.clockwise;
        speed = earth.speed;

        this.orbitingPlanet = earth;

    }

    public static SpaceMovementManager[] Planets()
    {
        return planets.ToArray();
    }

}
