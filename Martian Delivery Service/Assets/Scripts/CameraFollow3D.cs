﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow3D : MonoBehaviour {
    public float minViewDistance = 10;
    public float extraCameraPadding = 5;
    public Vector3 offsetDir;
    Camera cam;

	// Use this for initialization
	void Start () {
        cam = this.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        //OPTIMIZATION: Cache previous results
        List<Int2> focusLocations = new List<Int2>();

        if (Game.GetInstance().GetCurrentPlayer() != null)
        {
            

            
            focusLocations.Add(Game.GetInstance().GetCurrentPlayer().ship.location);
            focusLocations.Add(Game.GetInstance().GetCurrentPlayer().mission.GetFocusLocation().location);

            Route route = CardManager.GetInstance().predictedMovementManager.GetRoute();

            focusLocations.Add(route.waypoints[route.waypoints.Count - 1]);

            
        }
        else
        {

            foreach (Player player in Game.GetInstance().GetPlayers())
            {
                if (player.IsDead() || player.mission.IsComplete()) continue;
                focusLocations.AddRange(player.ship.GetRoute().waypoints);
            }

            

        }

        //focus the camera on the center

        Vector3 camPlaneNormal = cam.transform.forward;

        List<Vector3> focusLocations3D = new List<Vector3>();

        foreach (Int2 hex in focusLocations) focusLocations3D.Add(Vector3.ProjectOnPlane(HexUtils.HexToTransform(hex), camPlaneNormal));



        float maxX = float.MinValue;
        float minX = float.MaxValue;
        float maxY = float.MinValue;
        float minY = float.MaxValue;
        float maxZ = float.MinValue;
        float minZ = float.MaxValue;
        


        foreach (Vector3 loc in focusLocations3D)
        {
            maxX = Mathf.Max(loc.x, maxX);
            minX = Mathf.Min(loc.x, minX);

            maxY = Mathf.Max(loc.y, maxY);
            minY = Mathf.Min(loc.y, minY);

            maxZ = Mathf.Max(loc.z, maxZ);
            minZ = Mathf.Min(loc.z, maxZ);
        }

        maxX += extraCameraPadding;
        minX -= extraCameraPadding;
        maxY += extraCameraPadding;
        minY -= extraCameraPadding;
        maxZ += extraCameraPadding;
        minZ -= extraCameraPadding;

        Vector3 maxExtent = new Vector3(maxX, maxY, maxZ);

        Vector3 minExtent = new Vector3(minX, minY, minZ);

        Vector3 focusPoint = new Vector3((maxX + minX) / 2, (minY + maxY) / 2, (minZ + maxZ) / 2);
        
        float viewConeRadius = Vector3.Distance(maxExtent, minExtent) / 2;

        //now we konw the center of the cone and the radius of the cone.
        //we use the radius and the camera's opening angle (based on the aspect ratio)
        //to determine how far back we need to be to keep the focus points in view.

        float vertViewingAngle = cam.fieldOfView;
        float horizViewingAngle = Mathf.Rad2Deg * 2 * Mathf.Atan(Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad / 2)) * cam.aspect;

        float restrictiveViewingAngle = Mathf.Min(vertViewingAngle, horizViewingAngle);

        float coneHeight = viewConeRadius / Mathf.Tan(Mathf.Deg2Rad * restrictiveViewingAngle);
        coneHeight = Mathf.Max(minViewDistance, coneHeight);
        

        cam.transform.position = focusPoint + cam.transform.forward.normalized * -1 * coneHeight;

    }
}
