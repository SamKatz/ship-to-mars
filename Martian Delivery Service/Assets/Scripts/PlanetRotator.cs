﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotator : MonoBehaviour {

    public float degreesPerAnimationPeriod = 720;

    Quaternion baseRotation;

	// Use this for initialization
	void Start () {
        baseRotation = this.transform.rotation;
	}

    public void UpdateProgress(float progress)
    {
        this.transform.rotation = baseRotation;
        this.transform.Rotate(new Vector3(0, degreesPerAnimationPeriod * progress, 0));
    }

}
