﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TemporaryWinScript : MonoBehaviour {
    public static string winText = "";

    public Text winDisplay;
    

	// Use this for initialization
	void Start () {
        winDisplay.text = winText;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Restart()
    {
        Utils.Restart();
    }
}
