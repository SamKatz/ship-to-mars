﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreDisplay : MonoBehaviour {

    public static List<PlayerRecord> players = new List<PlayerRecord>();

    

    


    public Text nameList;
    public Text scoreList;

    string baseColor = "<color=white>";
    string closeColor = "</color>";
    string endl = "\r\n";

	// Use this for initialization
	void Start () {
        PlayerRecord[] playersToRecord = players.ToArray();

        players.Sort();
        

		foreach(int score in Game.highScores.Keys)
        {
            foreach(string name in Game.highScores[score])
            {
                for(int i = 0; i < players.Count; i++)
                {
                    PlayerRecord pl = players[i];
                    if (pl.score >= score)
                    {
                        String playerColor = Utils.colorToColorTag((Color) pl.color) + "<b>";

                        nameList.text += playerColor + pl.name + "</b>" + closeColor +  endl;
                        scoreList.text += playerColor + Utils.formatScoreToCash(pl.score) + "</b>" +  closeColor + endl;
                        players.Remove(pl);
                        i--;
                    }
                }

                nameList.text += baseColor + name + closeColor +  endl;
                scoreList.text += baseColor + Utils.formatScoreToCash(score) + closeColor + endl;


            }
        }

        foreach(PlayerRecord pl in players)
        {
            String playerColor = "<color=#" + ColorUtility.ToHtmlStringRGB((Color) pl.color) + "><b>";

            nameList.text += playerColor + pl.name + "</b>" + closeColor + endl;
            scoreList.text += playerColor + Utils.formatScoreToCash(pl.score) + "</b>" + closeColor + endl;
        }


        

        foreach (PlayerRecord pl in playersToRecord)
        {
            

            if (Game.highScores.ContainsKey(pl.score))
            {
                Game.highScores[pl.score].Add(pl.name);
            }else
            {
                Game.highScores.Add(pl.score, new List<string>());
                Game.highScores[pl.score].Add(pl.name);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
