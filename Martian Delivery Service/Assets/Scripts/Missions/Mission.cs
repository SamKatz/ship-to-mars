﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Mission {

    public readonly Player owner;

    public Nullable<int> turnCompleted;

    public Mission(Player player)
    {
        this.owner = player;
    }


    public abstract bool IsComplete();

    public virtual void TurnComplete()
    {
        if(IsComplete() && turnCompleted == null)
        {
            turnCompleted = Game.GetInstance().turnNumber;
        }
    }

    public abstract SpaceMovementManager GetFocusLocation();

    public abstract int GetBasePay();

    public virtual bool UseCargoDamage()
    {
        return true;
    }

    public int GetScore()
    {
        int score = 0;
        score -= GetRepairCost(owner.shipHealth);
        if(turnCompleted != null && owner.shipHealth > 0)
        {
            if (UseCargoDamage())
                score += (int) (GetBasePay() * (((double) owner.cargoHealth) / Player.maxCargoHealth));
            else score += GetBasePay();
            score += (Game.turnLimit - (int) turnCompleted);
            score += (int) (5 * (((double) owner.fuel) / Player.initialFuel));
        }
        return score;
    }

    public int GetRepairCost(int shipHealth)
    {
        switch (shipHealth)
        {
            case 4:
                return 0;
            case 3:
                return 21;
            case 2:
                return 34;
            case 1:
                return 55;
            case 0:
                return 89;
        }
        throw new InvalidOperationException("Invalid health value");
    }
}
