﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetToMarsMission : Mission
{
    public SpaceMovementManager mars;
    public GetToMarsMission(Player player) : base(player)
    {
        mars = GameObject.Find("Mars").GetComponent<SpaceMovementManager>();
    }

    public override int GetBasePay()
    {
        return 40;
    }

    public override SpaceMovementManager GetFocusLocation()
    {
        return mars;
    }

    public override bool IsComplete()
    {
        if(owner.ship != null && owner.shipHealth > 0 && owner.ship.orbitingPlanet != null && owner.ship.orbitingPlanet.name == "Mars")
        {
            return true;
        }
        
        return false;
    }

    public override bool UseCargoDamage()
    {
        return false;
    }
}
