﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Player : MonoBehaviour {

    public enum PlayerStatus
    {
        EnRoute, MissionComplete, DestroyedInManuever, DestroyedInOrbit, DestroytedInSun, OutOfRange, OutOfFuel
    }


    public PlayerStatus status = PlayerStatus.EnRoute;

    public const int initialFuel = 40;


    public bool hasPlayedEndCinematic = false;

    public string corpName;

    public const int maxShipHealth = 4;
    public const int maxCargoHealth = 4;

    public int shipHealth = 4;
    public int cargoHealth = 4;

    public int turnNumber; //place in the turn order from 0 to N.

    public Color color;

    public int fuel;

    public Mission mission;
    
    public SpaceMovementManager ship;

    public bool willDie = false;
    public bool willDamageShip = false;
    public bool willDamageCargo = false;

    public float totalTurnTime = 0;

    void Awake()
    {
        Game.GetInstance().RegisterPlayer(this);
        this.fuel = initialFuel;
        this.mission = new GetToMarsMission(this);
    }

    public void FinishMove()
    {

        Route route = ship.GetRoute(willDamageShip);

        if (route.isFatal)
        {
            cargoHealth = 0;
            shipHealth = 0;
            ship.Kill();

            if (route.entersSun) status = PlayerStatus.DestroytedInSun;
            if (route.exitsSolarSystem) status = PlayerStatus.OutOfRange;

            return;
        }
        else if (route.willDamageShip)
        {

            GoingToDamageShip();
        }

        if(this.fuel == 0)
        {
            cargoHealth = 0;
            shipHealth = 0;
            ship.Kill();

            status = PlayerStatus.OutOfFuel;
        }

        if (this.willDamageCargo)
        {
            this.DamageCargo();
            this.willDamageCargo = false;
        }
        if (this.willDamageShip)
        {
            this.DamageShip();
            this.willDamageShip = false;
            if (ship.IsDead())
            {
                if (route.sunDamage) status = PlayerStatus.DestroytedInSun;
                if (route.orbitDamage) status = PlayerStatus.DestroyedInOrbit;
                if (route.manueverDamage) status = PlayerStatus.DestroyedInManuever;
            }
        }
        if (this.willDie && !ship.IsDead())
        {
            ship.Kill();
            this.willDie = false;
        }
        ship.NextTurn();

        mission.TurnComplete();
        if (mission.IsComplete() && !ship.IsDead()) status = PlayerStatus.MissionComplete;

        

    }

    public bool IsDead()
    {
        return ship == null || ship.IsDead();
    }




    public bool HasFuel(int fuel)
    {
        return this.fuel >= fuel;
    }

    public void SpendFuel(int fuel)
    {
        this.fuel -= fuel;
    }

    public static string GetHealthDescription(int health)
    {
        switch (health)
        {
            case 4:
                return "OK";
            case 3:
                return "Stressed";
            case 2:
                return "Damaged";
            case 1:
                return "Critical";
            case 0:
                return "Destroyed";
            
        }
        return "INVALID";
    }

    public static string GetHealthColorTag(int health)
    {
        switch (health)
        {
            case 4:
                return "green";
            case 3:
                return "yellow";
            case 2:
                return "orange";
            case 1:
                return "red";
            case 0:
                return "grey";
        }
        return "magenta";
    }



    int[,] cargoDamageChart = new int[5, 6] { 
        {
            0,0,0,0,0,0 //0 health
        },
        {
            0,0,0,0,0,0 //1 health
        },
        {
            1,1,0,0,0,0 //2 health
        },
        {
            2,2,1,1,0,0 //3 health
        },
        {
            3,3,3,2,1,0 //4 health
        }
    };
    public void GoingToDamageCargo()
    {
        this.willDamageCargo = true;
    }
    
    public void DamageCargo()
    {
        int random = UnityEngine.Random.Range(0, 5);

        cargoHealth = cargoDamageChart[cargoHealth, random];
    }


    int[,] shipDamageChart = new int[5, 6] {
        {
            0,0,0,0,0,0 //0 health
        },
        {
            0,0,0,0,0,0 //1 health
        },
        {
            1,1,1,0,0,0 //2 health
        },
        {
            2,2,2,2,1,1 //3 health
        },
        {
            3,3,3,3,2,1 //4 health
        }
    };
    public void DamageShip()
    {
        int random = UnityEngine.Random.Range(0, 5);

        shipHealth = shipDamageChart[shipHealth, random];

        if (shipHealth == 0) ship.Kill();
    }

    public void GoingToDamageShip()
    {
        this.willDamageShip = true;
    }

    public void GoingToDie()
    {
        this.willDie = true;
    }


    public bool DeathPossibleOnNextDamage()
    {
        return shipDamageChart[shipHealth, 5] == 0;
    }

    public bool CargoFailPossibleOnNextDamage()
    {
        return cargoDamageChart[cargoHealth, 5] == 0;
    }
}
