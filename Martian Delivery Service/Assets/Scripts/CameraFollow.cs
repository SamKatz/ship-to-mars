﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    float viewDistance = 10;
    Camera cam;

    // Use this for initialization
	void Start () {
        cam = this.GetComponent<Camera>();
	}

    public float animatiionCameraPadding = 5;
    public float cameraPadding = 5;



	// Update is called once per frame
	void Update () {

        if (Game.GetInstance().GetCurrentPlayer() != null)
        {
            Vector3 campos = Game.GetInstance().GetCurrentPlayer().ship.gameObject.transform.position;
            campos.z = campos.z - viewDistance;
            gameObject.transform.position = campos;

            List<Int2> focusLocations = new List<Int2>();
            focusLocations.Add(Game.GetInstance().GetCurrentPlayer().ship.location);
            focusLocations.Add(Game.GetInstance().GetCurrentPlayer().mission.GetFocusLocation().location);

            Route route = CardManager.GetInstance().predictedMovementManager.GetRoute();

            focusLocations.Add(route.waypoints[route.waypoints.Count - 1]);
            focusLocations.Add(new Int2(0,0));

            HexUtils.SetViewAll(focusLocations, cam, animatiionCameraPadding);
        } else
        {
            List<Int2> locationList = new List<Int2>();

            foreach (Player player in Game.GetInstance().GetPlayers())
            {
                if (player.IsDead() || player.mission.IsComplete()) continue;
                locationList.AddRange(player.ship.GetRoute().waypoints);
            }
            if(locationList.Count >= 1) HexUtils.SetViewAll(locationList, cam, cameraPadding);

        }

        
	}
}
