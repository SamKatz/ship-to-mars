﻿using UnityEngine;
using System;
using UnityEngine.Networking;

public class PlayerRecord : IComparable
{

    public PlayerRecord(string storedRecord)
    {
        string[] splitRecord = storedRecord.Split('|');

        this.name = splitRecord[0];
        this.score = Int32.Parse(splitRecord[1]);

        this.color = null;

        this.status = (Player.PlayerStatus) Enum.Parse(typeof(Player.PlayerStatus), splitRecord[2]);
        this.numberOfTurns = Int32.Parse(splitRecord[3]);
        this.averageTurnLength = float.Parse(splitRecord[4]);

    }

    public PlayerRecord(string name, int score, Color color, Player.PlayerStatus status, int numberOfTurns, float averageTurnLength, int fuelRemaining, int shipHealthRemaining, int cargoHealthRemaining, Mission mission)
    {
        this.name = name;
        this.score = score;
        this.color = color;
        this.status = status;
        this.numberOfTurns = numberOfTurns;
        this.averageTurnLength = averageTurnLength;
        this.fuelRemaining = fuelRemaining;
        this.shipHealthRemaining = shipHealthRemaining;
        this.cargoHealthRemaining = cargoHealthRemaining;
        this.mission = mission;
    }

    public int score;

    public string name;
    public Nullable<Color> color; //null value used for stored records where we don't care about the color
    public Player.PlayerStatus status;
    public int numberOfTurns;
    public float averageTurnLength;
    public Mission mission;

    //only used for score breakdown, not for player aggregate stats.
    public int fuelRemaining;
    public int shipHealthRemaining;
    public int cargoHealthRemaining;


    public int CompareTo(object obj)
    {
        return ((PlayerRecord)obj).score.CompareTo(score);
    }

    public override string ToString()
    {
        return name + "|" + score + "|" + status + "|" + numberOfTurns + "|" + averageTurnLength;
    }

}