﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;


public class Game : MonoBehaviour
{
    private static Game instance;


    public GameObject[] shipPrefabs;

    

    public class ReverseComparer : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            return y.CompareTo(x);
        }


    }
    public static SortedDictionary<int, List<string>> highScores = new SortedDictionary<int, List<string>>(new ReverseComparer());

    public static int numPlayers = 1;
    public int orbitsPerRound = 2;
    public float animationLength = 5;
    public float endCinematicLength = 5.01f;
    public bool createPlayer = true;


    public int turnNumber = 0;

    public enum AnimationState
    {
        Normal, EndCinematic
    }


    public AnimationState currentState = AnimationState.Normal;

    public AnimationState GetCurrentAnimationState()
    {
        return currentState;
    }

    public Nullable<int> playerNumber = 0;

    private List<Player> turnOrder = new List<Player>();

    public static Color[] colors = { Color.green, new Color(1, 0.15f, 0), Color.cyan, Color.yellow };
    public Sprite[] predictorSprites = { };
   

    public const int turnLimit = 20;

    public const int maxPlayerRecords = 200;


    public float lastTurnStarted;

    public static string[] firstNames = new string[]
    {
        "Planet",
        "Galaxy",
        "Space",
        "Omni",
        "Aperture",
        "Robo",
        "Cyber",
        "InGen",
        "MultiNational",
        "Massive",
        "Mom's",
        "Star",
        "Venture",
        "Global",
        "Dharma",
        "Icarus"
    };

    public static string[] lastNames = new string[]
    {
        "Express",
        "Quest",
        "Enterprise",
        "X",
        "Federation",
        "Corp",
        "Science",
        "Mesa",
        "Delivery",
        "Sun",
        "Systems",
        "Tech",
        "United",
        "Dynamic",
        "Robots",
        "Helix",
        "Industries",
        "Dynamics",
        "Initiative"
    };

    public String GenerateNewName()
    {


        while (true)
        {

            String name = firstNames[UnityEngine.Random.Range(0, firstNames.Length)] + " " + lastNames[UnityEngine.Random.Range(0, lastNames.Length)];
            foreach(Player p in turnOrder)
            {
                if (p.corpName == name) continue;
            }
            return name;
        }
    }

    public static Game GetInstance()
    {
        return instance;
    }

    public Player GetCurrentPlayer()
    {
        if (playerNumber == null)
        {
            return null;
        }

        return turnOrder[(int)playerNumber];
    }


    public void RegisterPlayer(Player p)
    {
        turnOrder.Add(p);
    }

    public bool CheckIfEndGame()
    {
        if (turnNumber > turnLimit)
        {
            EndGame();
            return true;
        }
        foreach(Player player in turnOrder)
        {
            if (!player.IsDead() && !player.mission.IsComplete()) return false;
        }
        //TODO: better end of game behaviour
        EndGame();
        return true;
    }

    public void EndGame()
    {
        foreach (Player p in turnOrder) p.mission.TurnComplete();


        string wintext = "";

        for (int i = 0; i < turnOrder.Count; i++)
        {
            wintext += "Player " + (i + 1) + ": ";
            if (turnOrder[i].mission.IsComplete() && turnOrder[i].shipHealth > 0)
            {
                wintext += "Successfully orbiting Mars";
                if (turnOrder[i].cargoHealth > 0) wintext += " with cargo intact";
                else wintext += " although their cargo was destroyed";
            }
            else
            {
                wintext += "Mission failed";
            }
            wintext += "\r\n";

            

        }




        TemporaryWinScript.winText = wintext;


        HighScoreDisplay.players.Clear();
        foreach (Player p in turnOrder)
        {
            int turn;
            if (p.mission.IsComplete()) turn = (int) p.mission.turnCompleted;
            else turn = turnNumber;

            PlayerRecord playerRecord = new PlayerRecord(p.corpName, p.mission.GetScore(), p.color, p.status, turn, p.totalTurnTime / turn, p.fuel, p.shipHealth, p.cargoHealth, p.mission);

            if (p.mission.turnCompleted == null) playerRecord.shipHealthRemaining = 0;

            HighScoreDisplay.players.Add(playerRecord);

            RecordPlayerRecord(playerRecord);
            
        }


        SceneManager.LoadScene("breakdown scene");

        UpdateStats();

    }

    public float timeBeganEndCinematic
    {
        get;
        private set;
    }
    private void PlayEndCinematic(Player p)
    {
        p.hasPlayedEndCinematic = true;
        timeBeganEndCinematic = Time.time;
        currentState = AnimationState.EndCinematic;

        //determine what message to display

        foreach (Player otherplayer in turnOrder)
        {
            if (!otherplayer.IsDead() && otherplayer != p) otherplayer.ship.gameObject.SetActive(false);
        }


        string message = p.corpName + Environment.NewLine;

        if (!p.IsDead())
        {
            message += "Mission Successful: Orbit Achieved";
            p.ship.StartAnimation(false);

            Camera.main.GetComponent<CameraFollow>().enabled = false;

            List<Int2> ploclist = new List<Int2>();
            ploclist.Add(p.ship.location);

            HexUtils.SetViewAll(ploclist, Camera.main, 5);


        }
        else
        {
            message += "Mission Failed: Ship Lost";
            ToggleStaticBackground();
            this.Invoke("ToggleStaticBackground", endCinematicLength);
        }

        MessageManager.GetInstance().DisplayBigMessage(p, message, endCinematicLength);
        

        
        this.Invoke("NextPlayer", endCinematicLength);
    }



    public GameObject staticBackground;
    private void ToggleStaticBackground()
    {
        staticBackground.SetActive(!staticBackground.activeSelf);
    }

    public void NextPlayer()
    {
        PushUpdatePanel.GetInstance().CloseUpdateImmediate();

        foreach (Player player in turnOrder)
        {
            if(!player.IsDead()) player.ship.gameObject.SetActive(true);
        }
        SpaceMovementManager.StopAllAnimations();

        if (playerNumber == null)
        {
            if(currentState == AnimationState.Normal)
            {
                playerNumber = 0;
                FinishMoves();

                foreach (Player p in turnOrder)
                {
                    if ((p.IsDead() || p.mission.IsComplete()) && !p.hasPlayedEndCinematic)
                    {
                        playerNumber = null;
                        PlayEndCinematic(p);
                        return;
                    }
                }

                turnNumber++;
                if (CheckIfEndGame()) return;
            }else if(currentState == AnimationState.EndCinematic)
            {
                
                currentState = AnimationState.Normal;
                Camera.main.GetComponent<CameraFollow>().enabled = true;

                playerNumber = 0;

                foreach (Player p in turnOrder)
                {
                    if ((p.IsDead() || p.mission.IsComplete()) && !p.hasPlayedEndCinematic)
                    {
                        playerNumber = null;
                        PlayEndCinematic(p);
                        return;
                    }
                }

                turnNumber++;
                if (CheckIfEndGame()) return;
            }
            DrawHexes.GetInstance().BrightenHexes();

        }
        else
        {
            playerNumber++;

            if (playerNumber >= turnOrder.Count)
            {
                playerNumber = null;
                foreach (Player p in turnOrder)
                {
                    if (!p.IsDead()) p.ship.StartAnimation(false);
                }

                foreach (SpaceMovementManager planet in SpaceMovementManager.GetPlanets())
                {
                    planet.StartAnimation(false);
                }

                //stop animation period in a few seconds
                this.Invoke("NextPlayer", Game.GetInstance().animationLength);
                DrawHexes.GetInstance().DimHexes();
                return;
            }
        }

        if(GetCurrentPlayer().IsDead() || GetCurrentPlayer().mission.IsComplete())
        {      
            NextPlayer();
        }

        lastTurnStarted = Time.time;
    }

    public Player[] GetPlayers()
    {
        return turnOrder.ToArray();
    }

    public void FinishMoves()
    {
        foreach (SpaceMovementManager planet in SpaceMovementManager.Planets())
        {
            planet.NextTurn();
        }
        foreach (Player p in turnOrder)
        {
            if (p.IsDead()) continue;
            p.FinishMove();
        }
    }

    public void Awake()
    {

        SpaceMovementManager.CleanSpaceObjectLists();

        instance = this;
        if (createPlayer)
        {
            for (int i = 0; i < numPlayers; i++)
            {
                GameObject newPlayer = new GameObject();
                Player newPlayerComponent = newPlayer.AddComponent<Player>();
                newPlayerComponent.color = colors[i];
                newPlayerComponent.turnNumber = i;

                newPlayer.name = "Player " + (i + 1);

                newPlayerComponent.corpName = this.GenerateNewName();


                //find earth's location
                GameObject ship;
                if (i < shipPrefabs.Length) ship = Instantiate(shipPrefabs[i]);
                else ship = Instantiate(shipPrefabs[0]);

                //Utils.SetRenderColor(ship.GetComponent<Renderer>(), newPlayerComponent.color);
                ship.name = "Ship " + (i + 1);


                SpaceMovementManager shipComponent = ship.GetComponent<SpaceMovementManager>();

                newPlayerComponent.ship = shipComponent;

                shipComponent.ResetPositionToEarth();
            }
        }

        lastTurnStarted = Time.time;

    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            animationLength += 1;
        } else if (Input.GetKeyDown(KeyCode.F11))
        {
            animationLength -= 1;
        }

        if (animationLength == 0)
        {
            animationLength = 1;
        }
    }

    private static readonly string[] playerStatCategories =
    {
        "Players Who Started The Game",
        "Players Who Completed Their Mission Successfully",
        "Average Player's Turn Length",
        "Average Number of Turns Taken",
        "Average Score",
        "Number of players who were destroyed by flying too close to the sun",
        "Number of players who were destroyed by entering orbit at high speed",
        "Number of players who were destroyed by accelerating/deccelerating too quickly",
        "Number of players who were destroyed by flying outside of communication range",
        "Number of players whose ships were lost when they ran out of fuel",
        "Number of players who lost the game by taking too long to complete their mission",
        "Number of players who walked away from the game before winning or losing"
    };

    public void InitializeStats()
    {
        StreamWriter statsWriter = new StreamWriter("PlayerStats.txt");

        foreach (string stat in playerStatCategories)
        {
            statsWriter.WriteLine(stat + ": 0");
        }

        statsWriter.Close();
        
    }

    

    public static void ClearStats()
    {
        File.Delete("RawPlayerData.txt");
        File.Delete("PlayerStats.txt");
        EnsureFileExistence();
    }

    public static void EnsureFileExistence()
    {
        if (!File.Exists("RawPlayerData.txt")) File.Create("RawPlayerData.txt").Dispose();
        if (!File.Exists("PlayerStats.txt")) File.Create("PlayerStats.txt").Dispose();
    }

    public static void RecordPlayerRecord(PlayerRecord pr)
    {
        RecordString(pr.ToString());
    }

    public static void RecordString(string str)
    {
        EnsureFileExistence();
        StreamReader dataReader = new StreamReader("RawPlayerData.txt");

        string fileContents = "";

        for (int i = 0; i < maxPlayerRecords - 1 && !dataReader.EndOfStream; i++)
        {
            fileContents += dataReader.ReadLine() + Environment.NewLine;

        }

        dataReader.Close();

        fileContents = str + Environment.NewLine + fileContents;


        StreamWriter dataWriter = new StreamWriter("RawPlayerData.txt");
        dataWriter.Write(fileContents);

        dataWriter.Close();
    }


    public static void UpdateStats()
    {
        List<PlayerRecord> records = new List<PlayerRecord>();
        EnsureFileExistence();
        StreamReader dataReader = new StreamReader("RawPlayerData.txt");

        int startedPlayers = 0;
        int walkawayDeath = 0;

        while (!dataReader.EndOfStream)
        {
            startedPlayers++;
            string line = dataReader.ReadLine();

            if (line == "WALKAWAY") walkawayDeath++;
            else records.Add(new PlayerRecord(line));
        }

        
        int finishedPlayers = 0;
        float turnLengthTotal = 0;
        float numTurnsTotal = 0;
        float averageScore = 0;
        int sunDeath = 0;
        int orbitDeath = 0;
        int manueverDeath = 0;
        int exitMapDeath = 0;
        int timeOutDeath = 0;
        int fuelShortageDeath = 0;

        foreach (PlayerRecord rec in records)
        {
            turnLengthTotal += rec.averageTurnLength;
            numTurnsTotal += rec.numberOfTurns;
            averageScore += rec.score;

            finishedPlayers += rec.status == Player.PlayerStatus.MissionComplete ? 1 : 0;

            sunDeath += rec.status == Player.PlayerStatus.DestroytedInSun ? 1 : 0;
            orbitDeath += rec.status == Player.PlayerStatus.DestroyedInOrbit ? 1 : 0;
            manueverDeath += rec.status == Player.PlayerStatus.DestroyedInManuever ? 1 : 0;
            exitMapDeath += rec.status == Player.PlayerStatus.OutOfRange ? 1 : 0;
            timeOutDeath += rec.status == Player.PlayerStatus.EnRoute ? 1 : 0;
            fuelShortageDeath += rec.status == Player.PlayerStatus.OutOfFuel ? 1 : 0;
            
        }

        averageScore /= records.Count;
        turnLengthTotal /= records.Count;
        numTurnsTotal /= records.Count;

        object[] stats = { startedPlayers, finishedPlayers, turnLengthTotal, numTurnsTotal, averageScore, sunDeath, orbitDeath, manueverDeath, exitMapDeath, fuelShortageDeath, timeOutDeath, walkawayDeath };
        
        dataReader.Close();

        StreamWriter statsWriter = new StreamWriter("PlayerStats.txt");

        for (int i = 0; i < stats.Length; i++)
        {
            statsWriter.WriteLine(playerStatCategories[i] + ": " + stats[i]);
        }
        statsWriter.Close();
    }

}
    
