﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disabler : MonoBehaviour {

    public GameObject toDisable;
    public bool enableInstead = false;


	public void Disable()
    {
        toDisable.SetActive(enableInstead);
    }
}
