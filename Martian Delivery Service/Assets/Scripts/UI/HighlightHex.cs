﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightHex : MonoBehaviour
{

    public int playerNum = 0;

    private Player player;

	// Use this for initialization
	void Start ()
	{
	    if (playerNum + 1 > Game.numPlayers)
	    {
            this.gameObject.SetActive(false);
	        return;
	    }
	    player = Game.GetInstance().GetPlayers()[playerNum];
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (player.IsDead())
	    {
	        return;
	    }
	    this.transform.position = HexUtils.HexToTransform(player.ship.location);
	}
}
