﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerImageSwap : MonoBehaviour
{

    public Sprite defaultSprite;
    public Sprite[] playerImages;
    private Image image;

	// Use this for initialization
	void Start ()
	{
	    image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (Game.GetInstance().GetCurrentPlayer() == null) image.sprite = defaultSprite;
	    else image.sprite = playerImages[(int) Game.GetInstance().playerNumber];

	}
}
