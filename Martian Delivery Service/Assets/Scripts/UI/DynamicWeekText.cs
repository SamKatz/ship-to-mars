﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DynamicWeekText : MonoBehaviour
{
    Text text;
    public bool weeksLeft = true;
    public string prefix;
    public string suffix;

    // Use this for initialization
    void Start()
    {
        text = this.gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        int number = Game.GetInstance().turnNumber;
        if (weeksLeft) number = Game.turnLimit - number;
        text.text = prefix + number + suffix;
    }
}
