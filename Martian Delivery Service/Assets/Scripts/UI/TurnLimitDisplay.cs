﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnLimitDisplay : MonoBehaviour {
    Text text;
	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = "Turn " + Game.GetInstance().turnNumber + "/" + Game.turnLimit + "\r\n (" + (Game.turnLimit - Game.GetInstance().turnNumber) + " weeks remaining)";

	}
}
