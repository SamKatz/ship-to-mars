﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class Card : MonoBehaviour {

    private bool valid = true;
    [SerializeField]
    public Graphic readyOverlay;
    [SerializeField]
    public Graphic notReadyOverlay;
    public Text cardName;

    public Toggle toggle;

    public Image disableX;

    public virtual void Start()
    {

    }
    
    public virtual void Update()
    {


        //OPTIMIZE - caching
        if (Game.GetInstance().GetCurrentPlayer() != null)
        {
            valid = IsValid(Game.GetInstance().GetCurrentPlayer().ship);
            

        }
        else valid = false;

        



        if (valid && Game.GetInstance().GetCurrentPlayer().HasFuel(GetRequiredFuel(Game.GetInstance().GetCurrentPlayer().ship)))
        {
            toggle.interactable = true;
            /*this.gameObject.GetComponent<Image>().enabled = true;

            foreach(Graphic graphic in gameObject.GetComponentsInChildren<Graphic>())
            {
                graphic.enabled = true;
            }*/

            this.GetComponent<Image>().color = Color.white;

            cardName.text = this.GetName(Game.GetInstance().GetCurrentPlayer().ship);

            disableX.enabled = false;

        }
        else
        {
            toggle.isOn = false;

            

            toggle.interactable = false;
            /*this.gameObject.GetComponent<Image>().enabled = false;
            foreach (Graphic graphic in gameObject.GetComponentsInChildren<Graphic>())
            {
                graphic.enabled = false;
            }*/
            this.GetComponent<Image>().color = new Color(0.7f, 0.7f, 0.7f) ;

            disableX.enabled = true;
        }
        

        if (toggle.isOn)
        {
            if (this.ReadyToPlay())
            {
                toggle.graphic = readyOverlay;
                readyOverlay.gameObject.SetActive(true);
                notReadyOverlay.gameObject.SetActive(false);
            }
            else
            {
                toggle.graphic = notReadyOverlay;
                readyOverlay.gameObject.SetActive(false);
                notReadyOverlay.gameObject.SetActive(true);
            }
        }else
        {
            readyOverlay.gameObject.SetActive(false);
            notReadyOverlay.gameObject.SetActive(false);
        }


        
    }



    public abstract bool IsValid(SpaceMovementManager movement); //maybe make this apply to a player later on

    public abstract bool ReadyToPlay();

    public virtual void Apply(SpaceMovementManager movement)
    {
        movement.lastCard = this;
    }

    public abstract int GetRequiredFuel(SpaceMovementManager movement);

    public abstract string GetName(SpaceMovementManager movement);

    public virtual void ResetOptions()
    {
        toggle.isOn = false;
    }

    public virtual bool CouldDVDamageCargo(SpaceMovementManager movement)
    {
        return GetRequiredFuel(movement) >= 10;
    }

    public virtual bool CouldDVDamageShip(SpaceMovementManager movement)
    {
        return GetRequiredFuel(movement) >= 10;
    }

    public virtual void OnValueChanged() { }

}
