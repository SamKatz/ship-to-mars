﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBreakDownScript : MonoBehaviour
{

    public int playerNumberToDisplay = 0;

    public Color categoryColor = Color.white;
    public Color positiveColor = Color.white;
    public Color zeroColor = Color.white;
    public Color negativeColor = Color.red;

    public Text categoryText;
    public Text amountText;

    public bool cargoDamage = false;

    string closeColor = "</color>";

    // Use this for initialization
    void Start ()
	{
        if (playerNumberToDisplay + 1 > Game.numPlayers) return;


	    PlayerRecord record = HighScoreDisplay.players[playerNumberToDisplay];


        FormatNewLine(categoryText, amountText, "Mission Base Pay", record.mission.GetBasePay());
        if (record.mission.turnCompleted != null)
	    {
	        
	        if(cargoDamage) FormatNewLine(categoryText, amountText, "Cargo Damage", -record.mission.GetBasePay() + (int)(record.mission.GetBasePay() * (((double)record.cargoHealthRemaining) / Player.maxCargoHealth)));
	        FormatNewLine(categoryText, amountText, "Delivery Speed Bonus", Game.turnLimit - record.numberOfTurns);
	        FormatNewLine(categoryText, amountText, "Remaining Fuel Bonus", (int)(5 * (((double)record.fuelRemaining) / Player.initialFuel)));
	        FormatNewLine(categoryText, amountText, "Ship Repair Cost", -record.mission.GetRepairCost(record.shipHealthRemaining));
        }
	    else
	    {
	        if(cargoDamage) FormatNewLine(categoryText, amountText, "Cargo Damage", -record.mission.GetBasePay());
            else FormatNewLine(categoryText, amountText, "Mission Failure Penalty", -record.mission.GetBasePay());
	        FormatNewLine(categoryText, amountText, "Delivery Speed Bonus", 0);
	        FormatNewLine(categoryText, amountText, "Remaining Fuel Bonus", 0);
	        FormatNewLine(categoryText, amountText, "Ship Replacement Cost", -record.mission.GetRepairCost(record.shipHealthRemaining));
        }

        

        


	}

    private void FormatNewLine(Text itemCategories, Text itemQuantities, string category, int amount)
    {
        itemCategories.text += Utils.colorToColorTag(categoryColor) + category + closeColor + System.Environment.NewLine;

        Color col;
        if (amount > 0)
        {
            col = positiveColor;
        }else if (amount < 0)
        {
            col = negativeColor;
        }
        else
        {
            col = zeroColor;
        }

        itemQuantities.text += Utils.colorToColorTag(col) + Utils.formatScoreToCash(amount) + closeColor + System.Environment.NewLine;
        
    }


	// Update is called once per frame
	void Update () {
		
	}
}
