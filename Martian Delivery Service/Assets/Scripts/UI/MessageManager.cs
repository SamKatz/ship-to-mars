﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour {

    private static MessageManager instance;

    public Text bigMessage;

    public Color[] defaultPlayerColors;

    public void DisplayBigMessage(Player recipient, string text, float time)
    {
        bigMessage.text = text;
        bigMessage.color = defaultPlayerColors[recipient.turnNumber];
        this.Invoke("ClearBigMessage", time);
    }

    public void ClearBigMessage()
    {
        bigMessage.text = "";
    }

    public static MessageManager GetInstance()
    {
        return instance;
    }

	// Use this for initialization
	void Start () {
        instance = this;
	}
}
