﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateAnimationParameters : MonoBehaviour
{
    Animator animator;
    Card card;

	// Use this for initialization
	void Start ()
	{
	    animator = GetComponent<Animator>();
	    card = GetComponent<Card>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    int playerNum = -1;
	    if (Game.GetInstance().playerNumber != null) playerNum = (int) Game.GetInstance().playerNumber;

        animator.SetInteger("Player Number", playerNum);

        

        if(Utils.animatorHasParameter(animator, "Number of Players")) animator.SetInteger("Number of Players", Game.numPlayers);


        if (card != null)
	    {
            animator.SetBool("Card Selected", CardManager.GetInstance().GetSelectedCard() == card);
	    }

	}
}
