﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceDisplay : MonoBehaviour
{

    public SpaceMovementManager target;

    private Text text;

    public string prefix;
    public string suffix;
	// Use this for initialization
	void Start ()
	{
	    text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Game.GetInstance().GetCurrentPlayer() != null)
	    {
	        text.text = prefix + HexUtils.HexDistance(target.location,
	                        Game.GetInstance().GetCurrentPlayer().ship.location) + suffix;
	    }
	}
}
