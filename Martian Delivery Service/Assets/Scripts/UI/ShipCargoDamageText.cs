﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipCargoDamageText : MonoBehaviour
{
    Text textCom;
    // Use this for initialization
    void Start()
    {
        textCom = gameObject.GetComponent<Text>();
    }

    public bool showCargo = false;

    // Update is called once per frame
    void Update()
    {
        if (Game.GetInstance().GetCurrentPlayer() == null)
        {
            textCom.text = "";
            return;
        }
        string line1 = "<b>Ship:</b> <color=" + Player.GetHealthColorTag(Game.GetInstance().GetCurrentPlayer().shipHealth) + ">" + Player.GetHealthDescription(Game.GetInstance().GetCurrentPlayer().shipHealth) + "</color>";
        string line2 = "<b>Cargo:</b> <color=" + Player.GetHealthColorTag(Game.GetInstance().GetCurrentPlayer().cargoHealth) + ">" + Player.GetHealthDescription(Game.GetInstance().GetCurrentPlayer().cargoHealth) + "</color>";

        if (CardManager.GetInstance().GetActiveCard() != null && (CardManager.GetInstance().GetActiveCard().CouldDVDamageCargo(Game.GetInstance().GetCurrentPlayer().ship)))
        {
            if (Game.GetInstance().GetCurrentPlayer().DeathPossibleOnNextDamage())
            {
                line1 += "\r\n<color=red>Destruction Possible</color>";

            }else
            {
                line1 += "\r\n<color=red>Damage Possible</color>";
            }
            
        }

        if (CardManager.GetInstance().GetActiveCard() != null && CardManager.GetInstance().GetActiveCard().CouldDVDamageShip(Game.GetInstance().GetCurrentPlayer().ship)){
            if (Game.GetInstance().GetCurrentPlayer().CargoFailPossibleOnNextDamage())
            {
                line2 += "\r\n<color=red>Destruction Possible</color>";
            }
            else
            {
                line2 += "\r\n<color=red>Damage Possible</color>";
            }
        }



        textCom.text = line1 + "\r\n";

        if (showCargo) textCom.text += line2;


    }
}
