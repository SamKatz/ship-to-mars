﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawHexes : MonoBehaviour
{
    private static DrawHexes instance;

    // Use this for initialization
    public bool drawCoordinates = false;
    public Material mat;
    public Color c = Color.blue;


    public float opacityDimFactor = 3;
    

    public List<LineRenderer> hexList = new List<LineRenderer>();

    public float zLevel = -0.1f;
    public float baseAlpha = 0.1f;

    public void BrightenHexes()
    {
        foreach (LineRenderer lr in hexList)
        {
            lr.startColor = new Color(lr.startColor.r, lr.startColor.g, lr.startColor.b, lr.startColor.a * opacityDimFactor);
            lr.endColor = new Color(lr.endColor.r, lr.endColor.g, lr.endColor.b, lr.endColor.a * opacityDimFactor);
        }
    }

    public void DimHexes()
    {
        foreach (LineRenderer lr in hexList)
        {
            lr.startColor = new Color(lr.startColor.r, lr.startColor.g, lr.startColor.b, lr.startColor.a / opacityDimFactor);
            lr.endColor = new Color(lr.endColor.r, lr.endColor.g, lr.endColor.b, lr.endColor.a / opacityDimFactor);
        }
    }

    public static DrawHexes GetInstance()
    {
        return instance;
    }


    void Start ()
    {
        instance = this;

        float rt3 = Mathf.Sqrt(3);
        float hexHeight = rt3 * HexUtils.hexSide;
        

        List<Vector3> hexes = new List<Vector3>();
        
        for(int x = -1 * HexUtils.mapRadius; x <= HexUtils.mapRadius; x++)
        {
            for(int y = -1 * HexUtils.mapRadius; y <= HexUtils.mapRadius; y++)
            {
                int distanceFromCenter;

                if ((x > 0 && y > 0) || (x < 0 && y < 0))
                {
                    distanceFromCenter = Mathf.Abs(x + y);
                }
                else
                {
                    distanceFromCenter = Mathf.Max(Mathf.Abs(x), Mathf.Abs(y));
                }


                float alpha = 1;

                

                if (distanceFromCenter > HexUtils.mapRadius)
                {
                    continue; //this checks if these axial coords are outside the hex of hexes
                }

                if(distanceFromCenter == HexUtils.mapRadius)
                {
                    alpha = 0.25f;
                }else if(distanceFromCenter == HexUtils.mapRadius - 1)
                {
                    alpha = 0.5f;
                }else if(distanceFromCenter == HexUtils.mapRadius - 2)
                {
                    alpha = 0.75f;
                }



                Vector3 centerOfHex = new Vector3(HexUtils.hexSide * 3f / 2 * x,
                                                  HexUtils.hexSide * rt3 * ((float) y + ((float) x / 2f)),
                                                  0);
                Debug.Log(x + " " + y);

                hexes.Add(new Vector3(centerOfHex.x - 1f / 2 * HexUtils.hexSide, centerOfHex.y + 1f / 2 * hexHeight, zLevel));
                hexes.Add(new Vector3(centerOfHex.x + 1f / 2 * HexUtils.hexSide, centerOfHex.y + 1f / 2 * hexHeight, zLevel));
                hexes.Add(new Vector3(centerOfHex.x + HexUtils.hexSide, centerOfHex.y, zLevel));
                hexes.Add(new Vector3(centerOfHex.x + 1f / 2 * HexUtils.hexSide, centerOfHex.y - 1f / 2 * hexHeight, zLevel));
                hexes.Add(new Vector3(centerOfHex.x - 1f / 2 * HexUtils.hexSide, centerOfHex.y - 1f / 2 * hexHeight, zLevel));
                hexes.Add(new Vector3(centerOfHex.x - HexUtils.hexSide, centerOfHex.y, zLevel));

                hexes.Add(new Vector3(centerOfHex.x - 1f / 2 * HexUtils.hexSide, centerOfHex.y + 1f / 2 * hexHeight, zLevel));

                GameObject hex = new GameObject();
                hex.name = "Hex " + x + ", " + y;

                hex.gameObject.transform.SetParent(this.gameObject.transform);


                Color colorWithAlpha = c;
                colorWithAlpha.a = baseAlpha * alpha;

                LineRenderer lr = hex.AddComponent<LineRenderer>();
                lr.positionCount = hexes.Count;
                lr.SetPositions(hexes.ToArray());
                lr.startWidth = 0.05f;
                lr.endWidth = 0.05f;
                lr.startColor = colorWithAlpha;
                lr.endColor = colorWithAlpha;
                lr.material = mat;
                hexes.Clear();
                hexList.Add(lr);
                

            }
        }




        
    }


	
	// Update is called once per frame
	void Update () {
		
	}

    void OnGUI()
    {
        if (!drawCoordinates) return;
          

        for (int x = -1 * HexUtils.mapRadius; x <= HexUtils.mapRadius; x++)
        {
            for (int y = -1 * HexUtils.mapRadius; y <= HexUtils.mapRadius; y++)
            {
                if (Mathf.Abs(x + y) >= HexUtils.mapRadius + 1)
                {
                    continue; //this checks if these axial coords are outside the hex of hexes
                }

                float rt3 = Mathf.Sqrt(3);
                Vector3 centerOfHex = new Vector3(HexUtils.hexSide * 3f / 2 * x,
                                                  HexUtils.hexSide * rt3 * ((float)y + ((float)x / 2f)),
                                                  0);
                centerOfHex += new Vector3(-0.5f, 0.5f, 0);
                //UnityEditor.Handles.Label(centerOfHex, x + ", " + y);

            }
        }
    }
}
