﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPanelScript : MonoBehaviour {
    public int playerNumber = 0;
    public Text playerName;
	// Use this for initialization
	void Start () {
		if(Game.GetInstance().GetPlayers().Length - 1 < playerNumber)
        {
            this.gameObject.SetActive(false);
        }else
        {
            Player p = Game.GetInstance().GetPlayers()[playerNumber];
            playerName.text = "P" + (playerNumber + 1) + ": " + p.corpName;
            playerName.color = p.color;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
