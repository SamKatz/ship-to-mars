﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CommitButton : MonoBehaviour {

    public CardManager cardManager;
    Button button;
    Animator control;

    void Start()
    {
        button = GetComponent<Button>();
        control = GetComponent<Animator>();
    }

    void Update()
    {
        button.interactable = cardManager.GetActiveCard() != null;

        int playerNum;

        if (Game.GetInstance().playerNumber == null)
        {
            playerNum = -1;
        }
        else
        {
            playerNum = (int) Game.GetInstance().playerNumber;
        }

    }


    public void CommitToCard()
    {
        Card active = cardManager.GetActiveCard();
        if (active == null) return;
        Game.GetInstance().GetCurrentPlayer().SpendFuel(active.GetRequiredFuel(Game.GetInstance().GetCurrentPlayer().ship));
        if (active.CouldDVDamageCargo(Game.GetInstance().GetCurrentPlayer().ship))
        {
            Game.GetInstance().GetCurrentPlayer().GoingToDamageCargo();
        }
        if (active.CouldDVDamageShip(Game.GetInstance().GetCurrentPlayer().ship))
        {
            Game.GetInstance().GetCurrentPlayer().GoingToDamageShip();
        }



        active.Apply(Game.GetInstance().GetCurrentPlayer().ship);

        cardManager.ResetCards();

        Game.GetInstance().GetCurrentPlayer().totalTurnTime += Time.time - Game.GetInstance().lastTurnStarted;

        Game.GetInstance().NextPlayer();

    }
}
