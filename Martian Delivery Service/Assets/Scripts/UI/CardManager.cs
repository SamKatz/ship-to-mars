﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour {

    public Card[] cardsToCommit;

    public bool handlePrediction = true;

    public GameObject destinationMarkerPrefab;
    public GameObject destinationMovementPrefab;

    public SpaceMovementManager predictedMovementManager;
    public TemporaryDestinationMarker predictionMarker;

    public static CardManager inst;

    public static CardManager GetInstance()
    {
        return inst;
    }

    public void Start()
    {
        inst = this;


        

        GameObject newShip = Instantiate(destinationMovementPrefab);
        newShip.name = "PredictedMovement";
        predictedMovementManager = newShip.GetComponent<SpaceMovementManager>();


        GameObject destinationMarker = Instantiate(destinationMarkerPrefab);
        destinationMarker.name = "PredictedDestination";
        predictionMarker = destinationMarker.GetComponent<TemporaryDestinationMarker>();
        predictionMarker.movement = predictedMovementManager;
        predictedMovementManager.StartAnimation(true);
    }

    void Update()
    {
        if (!handlePrediction) return;

        if(Game.GetInstance().GetCurrentPlayer() == null)
        {
            predictedMovementManager.gameObject.SetActive(false);
            predictionMarker.gameObject.SetActive(false);
            return;
        }
        else
        {
            predictedMovementManager.gameObject.SetActive(true);
            predictionMarker.gameObject.SetActive(true);
        }


        predictedMovementManager.transform.localScale = Game.GetInstance().GetCurrentPlayer().ship.transform.localScale;
        //predictedMovementManager.GetComponentInChildren<MeshFilter>().mesh = Game.GetInstance().GetCurrentPlayer().ship.GetComponentInChildren<MeshFilter>().mesh;
        predictedMovementManager.GetComponentInChildren<SpriteRenderer>().sprite = Game.GetInstance().predictorSprites[Game.GetInstance().GetCurrentPlayer().turnNumber];


        predictedMovementManager.location = Game.GetInstance().GetCurrentPlayer().ship.location;
        predictedMovementManager.travelDir = Game.GetInstance().GetCurrentPlayer().ship.travelDir;
        //predictedMovementManager.clockwise = Game.GetInstance().GetCurrentPlayer().ship.clockwise;
        predictedMovementManager.speed = Game.GetInstance().GetCurrentPlayer().ship.speed;
        predictedMovementManager.displayDir = Game.GetInstance().GetCurrentPlayer().ship.displayDir;
        predictedMovementManager.orbitingPlanet = Game.GetInstance().GetCurrentPlayer().ship.orbitingPlanet;


        //predictedMovementManager.zLevel = Game.GetInstance().GetCurrentPlayer().ship.zLevel;

        Utils.SetAllRenderColors(predictedMovementManager.gameObject, Game.GetInstance().GetCurrentPlayer().color); //OPTIMIZATION
        //predictionMarker.color = Game.GetInstance().GetCurrentPlayer().color;

        Card active = GetActiveCard();
        if (active == null) return;

        active.Apply(predictedMovementManager);
    }

    public Card GetSelectedCard()
    {
        Card active = null;
        foreach (Card card in cardsToCommit)
        {
            if (card.toggle.isOn)
            {
                active = card;
                break;
            }
        }
        return active;
    }

    public Card GetActiveCard()
    {
        Card selected = GetSelectedCard();
        if (selected == null || !selected.ReadyToPlay()) return null;
        else return selected;
    }

    public void ResetCards()
    {
        foreach(Card c in cardsToCommit)
        {
            c.ResetOptions();
        }
    }
}
