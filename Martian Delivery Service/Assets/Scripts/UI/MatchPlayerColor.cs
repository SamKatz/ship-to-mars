﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchPlayerColor : MonoBehaviour
{

    public List<Graphic> graphics;

    public Color neutralColor;
    public Color[] playerColors; //set graphics to color 0 on player 0's turn, etc.



	// Update is called once per frame
	void Update () {
	    foreach (Graphic comp in graphics)
	    {
	        if (Game.GetInstance().playerNumber == null)
	        {
	            comp.color = neutralColor;
            }
	        else
	        {
	            comp.color = playerColors[(int) Game.GetInstance().playerNumber];
	        }
	    }
	}
}
