﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelSlider : MonoBehaviour {

    public Slider slider;

    void Start()
    {
        slider.maxValue = Player.initialFuel;
        slider.minValue = 0;
        slider.wholeNumbers = true;

    }

	
	// Update is called once per frame
	void Update () {
        slider.value = Game.GetInstance().GetCurrentPlayer().fuel;
	}
}
