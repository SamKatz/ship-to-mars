﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddressedCommandText: MonoBehaviour {
    Text text;
    public string suffix = "";
	// Use this for initialization
	void Start () {
        text = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Game.GetInstance().GetCurrentPlayer() != null)
        {
            text.text = "Player " + (((int) Game.GetInstance().playerNumber) + 1) + 
                        ", " + Game.GetInstance().GetCurrentPlayer().corpName + ", " + suffix;
        }
	}
}
