﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitOrbitCard : Card {

    public override void Apply(SpaceMovementManager movement)
    {
        movement.orbitingPlanet = null;
        movement.speed = 2;
    }

    public override string GetName(SpaceMovementManager movement)
    {
        return "Exit Orbit";
    }

    public override int GetRequiredFuel(SpaceMovementManager movement)
    {
        return 4;
    }

    public override bool IsValid(SpaceMovementManager movement)
    {
        return movement.orbitingPlanet != null;
    }

    public override bool ReadyToPlay()
    {
        return true;
    }

    
}
