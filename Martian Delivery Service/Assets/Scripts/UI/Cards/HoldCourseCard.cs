﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldCourseCard : Card {

    public override string GetName(SpaceMovementManager movement)
    {
        return "Hold Course";
    }

    public override int GetRequiredFuel(SpaceMovementManager movement)
    {
        return 0;
    }

    public override bool IsValid(SpaceMovementManager movement)
    {
        return movement.orbitingPlanet == null;
    }

    public override bool ReadyToPlay()
    {
        return true;
    }
}
