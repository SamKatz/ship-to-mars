﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitCard : Card
{

    public override void Apply(SpaceMovementManager movement)
    {
        base.Apply(movement);

        movement.orbitingPlanet = SpaceMovementManager.GetPlanet(movement.location);
        movement.speed = movement.orbitingPlanet.speed;
        movement.displayDir = movement.orbitingPlanet.displayDir;
        movement.travelDir = movement.orbitingPlanet.travelDir;
    }

    public override string GetName(SpaceMovementManager movement)
    {
         return "Enter Orbit";
    }

    public override int GetRequiredFuel(SpaceMovementManager movement)
    {
        if (movement.orbitingPlanet == null)
        {
            switch (movement.speed)
            {
                case 1:
                    return 1;
                case 2:
                    return 3;
                case 3:
                    return 6;
            }
            return int.MaxValue;
        }
        else
        {
            return 4;
        }
    }
    public override bool IsValid(SpaceMovementManager movement)
    {
        return SpaceMovementManager.GetPlanet(movement.location) != null && movement.orbitingPlanet == null && movement.speed <= 3;
    }

    public override bool CouldDVDamageCargo(SpaceMovementManager movement)
    {
        return movement.speed >= 3;
    }

    public override bool CouldDVDamageShip(SpaceMovementManager movement)
    {
        return movement.speed >= 3;
    }

    public override bool ReadyToPlay()
    {
        return true;
    }

}
