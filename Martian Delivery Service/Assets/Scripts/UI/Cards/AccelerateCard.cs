﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccelerateCard : Card
{
    public int[] toggleValues;
    public Toggle[] toggles;
    public int[] dvCosts;

    public override void Start()
    {
        base.Start();

        this.toggle.onValueChanged.AddListener((value) =>

        {
            if (!this.toggle.isOn)
            {
                foreach (Toggle tog in toggles)
                {
                    tog.isOn = false;
                }
            }
        }
        );


    }

    public Nullable<int> GetToggleValue()
    {
        for (int i = 0; i < toggles.GetLength(0); i++)
        {
            if (toggles[i].isOn)
            {
                return toggleValues[i];
            }

        }
        return null;
    }

    public override void Update()
    {
        base.Update();

        if (!this.toggle.isOn)
        {
            foreach (Toggle tog in toggles)
            {
                tog.gameObject.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < toggles.GetLength(0); i++)
            {
                if (Game.GetInstance().GetCurrentPlayer() == null || toggleValues[i] + Game.GetInstance().GetCurrentPlayer().ship.speed <= 0 || dvCosts[i] > Game.GetInstance().GetCurrentPlayer().fuel)
                {
                    toggles[i].gameObject.SetActive(false);
                    toggles[i].isOn = false;
                }
                else
                {
                    toggles[i].gameObject.SetActive(true);

                }
            }

            for (int i = 0; i < toggles.GetLength(0); i++)
            {
                if (toggles[i].isOn && !toggle.isOn)
                {
                    toggle.isOn = true;

                }
            }

        }









    }
    public override void Apply(SpaceMovementManager movement)
    {
        base.Apply(movement);

        movement.speed += (int)GetToggleValue();
    }

    public override bool IsValid(SpaceMovementManager movement)
    {
        if (movement.orbitingPlanet != null) return false;
        for (int i = 0; i < toggleValues.GetLength(0); i++)
        {
            if (toggleValues[i] + movement.speed > 0 && dvCosts[i] <= Game.GetInstance().GetCurrentPlayer().fuel)
            {
                return true;
            }
        }

        return false;

    }

    public override bool ReadyToPlay()
    {
        return GetToggleValue() != null;
    }

    public override void ResetOptions()
    {
        base.ResetOptions();

        foreach (Toggle toggle in toggles)
        {
            toggle.isOn = false;
        }

    }

    public override int GetRequiredFuel(SpaceMovementManager movement)
    {
        for (int i = 0; i < toggles.GetLength(0); i++)
        {
            if (toggles[i].isOn)
            {
                return dvCosts[i];
            }

        }
        return 0;
    }

    public override string GetName(SpaceMovementManager movement)
    {
        if (toggleValues[0] > 0)
        {
            return "Accelerate";
        }
        else
        {
            return "Deccelerate";
        }
    }


}
