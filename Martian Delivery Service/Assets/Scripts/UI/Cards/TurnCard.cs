﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnCard : Card
{
    public bool turnClockwise = true;

    public override void Apply(SpaceMovementManager movement)
    {
        base.Apply(movement);

        if (turnClockwise)
        {
            movement.travelDir = HexUtils.NextClockwise(movement.travelDir);
        }else
        {
            movement.travelDir = HexUtils.NextCounterclockwise(movement.travelDir);
        }
    }

    public override bool IsValid(SpaceMovementManager movement)
    {
        /*if (movement.orbitingPlanet != null) return false;
        if (turnClockwise)
        {
            return HexUtils.IsTurnAllowed(movement.location, movement.travelDir, HexUtils.NextClockwise(movement.travelDir));
        }else
        {
            return HexUtils.IsTurnAllowed(movement.location, movement.travelDir, HexUtils.NextCounterclockwise(movement.travelDir));
        }*/
        return movement.orbitingPlanet == null;
    }

    public override bool ReadyToPlay()
    {
        return true;
    }

    public override int GetRequiredFuel(SpaceMovementManager movement)
    {
        return 1;
    }

    public override string GetName(SpaceMovementManager movement)
    {
        if (turnClockwise)
        {
            return "Turn Right";
        }
        else
        {
            return "Turn Left";
        }
    }
}
