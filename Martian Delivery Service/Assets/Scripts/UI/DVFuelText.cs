﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DVFuelText : MonoBehaviour {

    Text fueltext;
    public CardManager cardManager;


    
    // Use this for initialization
	void Start () {
        fueltext = GetComponent<Text>();
        fueltext.supportRichText = true;
        
	}
		// Update is called once per frame

	void Update () {

	    string defaultColorTag = "<color=#" + ColorUtility.ToHtmlStringRGB(fueltext.color).ToLower() + ">";

        if (Game.GetInstance().GetCurrentPlayer() == null)
        {
            fueltext.text = "";
            return;
        }
        string line1 = defaultColorTag + "<b>Fuel</b>:" + Game.GetInstance().GetCurrentPlayer().fuel + "</color>";

        Card active = cardManager.GetActiveCard();

        if(active != null)
        {
            int cost = active.GetRequiredFuel(Game.GetInstance().GetCurrentPlayer().ship);

            line1 += defaultColorTag + "-</color><color=red>" + cost + "</color>" + defaultColorTag + " =</color>" + (Game.GetInstance().GetCurrentPlayer().fuel - cost);
        }

        fueltext.text = line1;
    }
}
