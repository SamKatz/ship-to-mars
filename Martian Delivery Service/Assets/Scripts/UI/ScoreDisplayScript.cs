﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplayScript : MonoBehaviour
{
    private Text text;

    public int playerIndex;

	// Use this for initialization
	void Start ()
	{
        Debug.Log("New Code");

        if (playerIndex + 1 > Game.numPlayers) return;

        text = GetComponent<Text>();

	    text.text = Utils.formatScoreToCash(HighScoreDisplay.players[playerIndex].score);
	}
}
