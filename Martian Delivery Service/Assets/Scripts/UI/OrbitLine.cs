﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrbitLine : MonoBehaviour {
    public int radius;
    LineRenderer lr;
	// Use this for initialization
	void Start () {
        lr = this.gameObject.GetComponent<LineRenderer>();
        lr.positionCount = 6;
        int position = 0;
        foreach(int dir in System.Enum.GetValues(typeof(HexUtils.Direction))){ //for each direction
            lr.SetPosition(position, HexUtils.HexToTransform(radius * HexUtils.GetCoordinateVector((HexUtils.Direction) dir)));
            position++;
        }


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
