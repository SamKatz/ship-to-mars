﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedPanel : MonoBehaviour {

    Text speed;

	// Use this for initialization
	void Start () {
        speed = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Game.GetInstance().GetCurrentPlayer() == null) speed.text = "";
        else
        {
            speed.text = "<b>Speed: </b>" + Game.GetInstance().GetCurrentPlayer().ship.speed;

            if(CardManager.GetInstance().GetActiveCard() != null)
            {
                speed.text += " → " + CardManager.GetInstance().predictedMovementManager.speed;
            }

        }



	}
}
