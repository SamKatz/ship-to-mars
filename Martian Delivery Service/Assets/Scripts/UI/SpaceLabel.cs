﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceLabel : MonoBehaviour {
    public enum HAnchorOption
    {
        Left, Right, Middle
    }

    public enum VAnchorOption
    {
        Top, Middle, Bottom
    }

    private RectTransform label;

    public GameObject follow;
    public int followPlayer = -1;

    public bool fadeDuringCardSelect = false;
    public float contextAlpha = 0.3f;
    
    // Use this for initialization
	void Start ()
	{
        label = this.GetComponent<RectTransform>();

	    if (followPlayer != -1)
	    {
	        if (followPlayer + 1 > Game.numPlayers) this.gameObject.SetActive(false);
	        else follow = Game.GetInstance().GetPlayers()[followPlayer].ship.gameObject;
	    }

    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (follow == null)
	    {
            Destroy(this.gameObject);
            return;
	    }

        //TODO: Complete code to avoid label overlap.
        /*
	    if (Game.GetInstance().GetCurrentPlayer() != null &&
	        Game.GetInstance().GetCurrentPlayer().turnNumber == followPlayer)
	    {
	        int highestIndex = Int32.MaxValue;
	        SpaceLabel highestLabel;
	        int lowestIndex = 0;

	        foreach (SpaceLabel spaceLabel in avoidOverlapOnTurn)
	        {
                if(spaceLabel.)
	        }
        }*/

	    

	    if (fadeDuringCardSelect)
	    {
	        if (CardManager.GetInstance().GetActiveCard() != null)
	        {
	            foreach (Graphic graphic in this.GetComponentsInChildren<Graphic>())
	            {
	                graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, contextAlpha);
	            }
	        }
	        else
	        {
	            foreach (Graphic graphic in this.GetComponentsInChildren<Graphic>())
	            {
	                graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, 1);
	            }
	        }
        }

	    

	    Vector3 followPos = follow.transform.position;
        
	    Vector3 screenPoint = Camera.main.WorldToScreenPoint(followPos);

        Vector2 UIPoint = new Vector2(screenPoint.x, screenPoint.y);

        Vector2 UIPointAsPercentageOfScreen = new Vector2(UIPoint.x / Screen.width, UIPoint.y / Screen.height);

	    label.anchorMin = UIPointAsPercentageOfScreen;
	    label.anchorMax = UIPointAsPercentageOfScreen;

	    /*//set x position
        switch (HorizontalAnchor)
        {
            case HAnchorOption.Left:
                label.offsetMin = new Vector2(UIPoint.x + offset.x, label.offsetMin.y);
                break;
            case HAnchorOption.Middle:
                label.anchoredPosition = new Vector2(UIPoint.x + offset.x, label.anchoredPosition.y);
                break;
            case HAnchorOption.Right:
                label.offsetMax = new Vector2(UIPoint.x + offset.x, label.offsetMax.y);
                break;
        }
        //set y position
        switch (VerticalAnchor)
        {
            case VAnchorOption.Bottom:
                label.offsetMin = new Vector2(label.offsetMin.x, UIPoint.y + offset.y);
                break;
            case VAnchorOption.Middle:
                label.anchoredPosition = new Vector2(label.anchoredPosition.x, UIPoint.y + offset.y);
                break;
            case VAnchorOption.Top:
                label.offsetMax = new Vector2(label.offsetMax.x, UIPoint.y + offset.y);
                break;
        }*/



	}
}
