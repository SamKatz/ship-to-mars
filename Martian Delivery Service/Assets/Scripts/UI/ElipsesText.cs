﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElipsesText : MonoBehaviour {

    Text text;
    public string initialText;
    public int elipses = 3;
    public bool appendCardName;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        int numelipses = (int)Time.time;
        numelipses = numelipses % (elipses + 1);

        text.text = initialText;

	    if (appendCardName && Game.GetInstance().GetCurrentPlayer() != null && CardManager.GetInstance().GetSelectedCard() != null)
	    {
	        text.text += CardManager.GetInstance().GetSelectedCard().GetName(Game.GetInstance().GetCurrentPlayer().ship);
	    }

        for (int i = 0; i < numelipses; i++) text.text += ".";
	}
}
