﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PushUpdate : MonoBehaviour
{
    public enum Priority
    {
        Low=0, Normal=1, High=2
    }


    public List<Player> seenAlready;

    public abstract string GetText(); //use ### for player corp name

    public abstract bool OkForDisplay();

    public virtual Priority GetPriority()
    {
        return Priority.Normal;
    }
}
