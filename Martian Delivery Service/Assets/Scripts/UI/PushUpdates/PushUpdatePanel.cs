﻿using System.Collections;
using System.Collections.Generic;
using System.Deployment.Internal;
using UnityEngine;
using UnityEngine.UI;

public class PushUpdatePanel : MonoBehaviour
{

    public Text displaySection;

    public List<PushUpdate> updatesToDisplay = new List<PushUpdate>();

    public Button dismissButton;

    bool on = false;

    private static PushUpdatePanel instance;

    public static PushUpdatePanel GetInstance()
    {
        return instance;
    }

    void Start()
    {

        
        CloseUpdateImmediate();
        updatesToDisplay.AddRange(this.GetComponents<PushUpdate>());

        instance = this;
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (Game.GetInstance().GetCurrentPlayer() == null) return;

	    if (!on)
	    {

            List<PushUpdate> candidates = new List<PushUpdate>();

	        foreach (PushUpdate update in updatesToDisplay)
	        {
	            if (update.OkForDisplay())
	            {
	                candidates.Add(update);
	            }
	        }

	        bool done = false;
	        for (int i = 2; i >= 0 && !done; i--)
	        {
	            foreach (PushUpdate candidate in candidates)
	            {
	                if ((int) candidate.GetPriority() == i)
	                {
	                    if (candidate.seenAlready.Contains(Game.GetInstance().GetCurrentPlayer())) continue;

                        ShowUpdate(candidate);
	                    done = true;
	                    break;
	                }
	            }
	        }

        }
	}

    public void CloseUpdateImmediate()
    {
        Debug.Log("Closing push notification.");
        Utils.DisableAllBehaviours<Graphic>(this.gameObject);
        on = false;
        dismissButton.enabled = false;

    }

    void CloseUpdate()
    {
        CloseUpdate(Game.GetInstance().GetCurrentPlayer(), Game.GetInstance().turnNumber);
    }

    void CloseUpdate(Player p, int turn)
    {
        if (Game.GetInstance().turnNumber == turn && Game.GetInstance().GetCurrentPlayer() == p)
        {
            CloseUpdateImmediate();
        }
    }

    private float expireTime = 10f;

    void ShowUpdate(PushUpdate update)
    {
        on = true;
        Utils.EnableAllBehaviours<Graphic>(this.gameObject);
        displaySection.text = update.GetText().Replace("###", Game.GetInstance().GetCurrentPlayer().corpName);

        update.seenAlready.Add(Game.GetInstance().GetCurrentPlayer());
        dismissButton.enabled = true;
        //this.Invoke("CloseUpdate", expireTime);
    }

}
