﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class DistanceUpdate : PushUpdate
{

    public int distance;

    public string text;

    public SpaceMovementManager mars;

    public override string GetText()
    {
        return text;
    }

    public override bool OkForDisplay()
    {
        return HexUtils.HexDistance(Game.GetInstance().GetCurrentPlayer().ship.location, mars.location) <= distance;
    }
}
