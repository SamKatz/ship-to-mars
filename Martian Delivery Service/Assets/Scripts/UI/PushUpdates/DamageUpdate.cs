﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUpdate : PushUpdate {
    public override string GetText()
    {
        return
            "Observers are concerned as it is reported the ### ship suffered damage as the result of accerlating or deccelerating too quickly, or flying too close to the sun.";
    }

    public override bool OkForDisplay()
    {
        return Game.GetInstance().GetCurrentPlayer().shipHealth < Player.maxShipHealth;
    }

    public override Priority GetPriority()
    {
        return Priority.High;
    }
}
