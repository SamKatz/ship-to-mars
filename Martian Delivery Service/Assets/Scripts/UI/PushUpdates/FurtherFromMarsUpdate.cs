﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurtherFromMarsUpdate : PushUpdate {


    Dictionary<Player, int> closestPosToMars = new Dictionary<Player, int>();

    private SpaceMovementManager mars;

    void Start()
    {
        mars = GameObject.Find("Mars").GetComponent<SpaceMovementManager>();
    }

    void Update()
    {
        Player p = Game.GetInstance().GetCurrentPlayer();
        if(p == null) return;

        int mDist = HexUtils.HexDistance(p.ship.location, mars.location);

        if (!closestPosToMars.ContainsKey(p) || mDist < closestPosToMars[p])
        {
            if (closestPosToMars.ContainsKey(p))
            {
                closestPosToMars[p] = HexUtils.HexDistance(p.ship.location, mars.location);
            }
            else
            {
                closestPosToMars.Add(p, HexUtils.HexDistance(p.ship.location, mars.location));
            }
        }
    }

    public override string GetText()
    {
        return "Worry mounts as the ### ship flies farther away from Mars.";
    }

    public override bool OkForDisplay()
    {
        if (!closestPosToMars.ContainsKey(Game.GetInstance().GetCurrentPlayer())) return false;

        return HexUtils.HexDistance(Game.GetInstance().GetCurrentPlayer().ship.location, mars.location) >
               closestPosToMars[Game.GetInstance().GetCurrentPlayer()];
    }
}
