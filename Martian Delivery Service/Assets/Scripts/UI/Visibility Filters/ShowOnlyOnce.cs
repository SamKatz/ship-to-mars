﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShowOnlyOnce : VisibilityFilter
{

    VisibilityManager man;

    public Nullable<int>[] shownTurn;

    public override bool ShouldBeVisible()
    {
        if (Game.GetInstance().GetCurrentPlayer() == null) return false;
        int playerNo = (int) Game.GetInstance().playerNumber;

        if(shownTurn[playerNo] == null)
        {
            return true;
        }
        else if(shownTurn[playerNo] == Game.GetInstance().turnNumber)
        {
            return true;
        }
        else return false;
    }

    void Start()
    {
        man = this.gameObject.GetComponent<VisibilityManager>();
        shownTurn = new int?[Game.numPlayers];

    }

    void Update()
    {
        if (Game.GetInstance().GetCurrentPlayer() == null) return;
        int playerNo = (int)Game.GetInstance().playerNumber;

        if (man.Visible() && shownTurn[playerNo] == null) shownTurn[playerNo] = Game.GetInstance().turnNumber;
    }

}
