﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class VisibilityFilter : MonoBehaviour {

    public bool invert = false;

    public abstract bool ShouldBeVisible();
    
}
