﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleOnlyIfOtherObjectActive : VisibilityFilter
{
    public GameObject other;

    public override bool ShouldBeVisible()
    {
        return other.activeSelf;
    }
}
