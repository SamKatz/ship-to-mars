﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyOnSpeed : VisibilityFilter {
    public int minSpeed;
    public int maxSpeed;


    public override bool ShouldBeVisible()
    {
        if (Game.GetInstance().GetCurrentPlayer() == null || Game.GetInstance().GetCurrentPlayer().ship == null || Game.GetInstance().GetCurrentPlayer().ship.IsDead()) return false;
        else
        {
            int speed = Game.GetInstance().GetCurrentPlayer().ship.speed;

            return speed >= minSpeed && speed <= maxSpeed;

        }
    }
}
