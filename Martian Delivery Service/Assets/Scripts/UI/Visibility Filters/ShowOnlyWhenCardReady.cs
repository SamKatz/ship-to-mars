﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowOnlyWhenCardReady: VisibilityFilter {
    public override bool ShouldBeVisible()
    {
        return CardManager.GetInstance().GetActiveCard() != null && CardManager.GetInstance().GetActiveCard().ReadyToPlay();
    }
}
