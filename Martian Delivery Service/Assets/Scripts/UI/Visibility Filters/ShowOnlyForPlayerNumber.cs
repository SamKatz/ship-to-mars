﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyForPlayerNumber : VisibilityFilter
{

    public int ShowForPlayerNumber = 0;
    public bool ShowDuringAnimation = false;


    public override bool ShouldBeVisible()
    {
        return Game.GetInstance().playerNumber == ShowForPlayerNumber ||
               (Game.GetInstance().playerNumber == null && ShowDuringAnimation);
    }
}
