﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*Handles visibility of UI components based on Visibility Filters. 
Object will be visible if all filters fulfilled
*/

public class VisibilityManager : MonoBehaviour {

    VisibilityFilter[] filters;

    public bool visible { get; private set; }

    public bool delayVisibilityCheck = false;


    // Use this for initialization
	void Start () {
        filters = GetComponents<VisibilityFilter>();
	}
	
	// Update is called once per frame
	void Update () {
        if(!delayVisibilityCheck) DetermineVisibility();
	}

    void LateUpdate()
    {
        if(delayVisibilityCheck) DetermineVisibility();
    }

    void DetermineVisibility()
    {

        if (this.transform.parent != null)
        {
            foreach (VisibilityManager parentManagers in GetComponentsInParent<VisibilityManager>())
            {
                if (parentManagers == this) continue;
                if (!parentManagers.visible)
                {
                    visible = false;
                    return;
                }
            }
        }

        foreach (VisibilityFilter vfi in filters)
        {
            if ((!vfi.ShouldBeVisible() && !vfi.invert) || (vfi.ShouldBeVisible() && vfi.invert))
            {
                Utils.DisableAllBehaviours<Graphic>(this.gameObject);
                Utils.DisableAllRenderers(this.gameObject);
                visible = false;
                return;
            }
        }
        Utils.EnableAllBehaviours<Graphic>(this.gameObject);
        Utils.EnableAllRenderers(this.gameObject);
        visible = true;
    }

    public bool Visible()
    {
        return visible;
    }
}
