﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowOnlyOnTurn : VisibilityFilter {
    public int minTurn;
    public int maxTurn;
    public bool showDuringShipMovement = false;

    public override bool ShouldBeVisible()
    {
        if (Game.GetInstance().GetCurrentPlayer() == null && !showDuringShipMovement) return false;

        return Game.GetInstance().turnNumber >= minTurn && Game.GetInstance().turnNumber <= maxTurn;
    }
}
