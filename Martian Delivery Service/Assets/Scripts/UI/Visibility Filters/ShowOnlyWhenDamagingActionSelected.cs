﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyWhenDamagingActionSelected : VisibilityFilter
{

    public bool activeForCargoDamage = false;
    public bool activeForShipDamage = false;

    public override bool ShouldBeVisible()
    {
        return CardManager.GetInstance().GetActiveCard() != null && Game.GetInstance().GetCurrentPlayer() != null &&
            ((CardManager.GetInstance().GetActiveCard().CouldDVDamageCargo(Game.GetInstance().GetCurrentPlayer().ship) && activeForCargoDamage) ||
            (CardManager.GetInstance().GetActiveCard().CouldDVDamageShip(Game.GetInstance().GetCurrentPlayer().ship) && activeForShipDamage));
    }
}
