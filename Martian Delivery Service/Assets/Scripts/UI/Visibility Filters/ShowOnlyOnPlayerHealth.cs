﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyOnPlayerHealth : VisibilityFilter
{

    public int maxHealth;
    public int minHealth;

    public override bool ShouldBeVisible()
    {
        Player p = Game.GetInstance().GetCurrentPlayer();

        return p != null && p.shipHealth <= maxHealth && p.shipHealth >= minHealth;
    }
}
