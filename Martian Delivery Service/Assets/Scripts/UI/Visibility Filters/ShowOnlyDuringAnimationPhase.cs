﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyDuringAnimationPhase : VisibilityFilter
{
    public override bool ShouldBeVisible()
    {
        return Game.GetInstance().GetCurrentPlayer() == null;
    }
}
