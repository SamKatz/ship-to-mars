﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyInCinematic : VisibilityFilter {
    public override bool ShouldBeVisible()
    {
        return Game.GetInstance().GetCurrentAnimationState() == Game.AnimationState.EndCinematic;
    }
}
