﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleOnlyIfCardValid : VisibilityFilter
{
    [SerializeField]
    Card card;

	// Use this for initialization
	void Start ()
	{
	    if (card == null) card = GetComponent<Card>();
	}

    public override bool ShouldBeVisible()
    {
        if (Game.GetInstance().GetCurrentPlayer() == null) return false;
        if (Game.GetInstance().GetCurrentPlayer().ship == null) return false;

        return card.IsValid(Game.GetInstance().GetCurrentPlayer().ship);
    }
}
