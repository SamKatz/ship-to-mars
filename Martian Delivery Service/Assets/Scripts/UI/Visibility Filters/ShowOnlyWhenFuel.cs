﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyWhenFuel : VisibilityFilter {

    public int minFuel;
    public int maxFuel;

    public override bool ShouldBeVisible()
    {
        Player p = Game.GetInstance().GetCurrentPlayer();

        return p != null && p.fuel >= minFuel && p.fuel <= maxFuel;

    }

    
}
