﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyWithNumberOfPlayers : VisibilityFilter {
    public int minPlayers = 0;
    public int maxPlayers = 2;


    public override bool ShouldBeVisible()
    {
        return Game.numPlayers <= maxPlayers && Game.numPlayers >= minPlayers;
    }

}
