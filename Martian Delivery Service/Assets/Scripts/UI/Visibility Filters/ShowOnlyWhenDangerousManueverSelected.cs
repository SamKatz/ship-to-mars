﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnlyWhenDangerousManueverSelected : VisibilityFilter {
    public override bool ShouldBeVisible()
    {

        Player p = Game.GetInstance().GetCurrentPlayer();
        Card card = CardManager.GetInstance().GetActiveCard();
        if (p == null || card == null) return false;
        else return card.CouldDVDamageShip(p.ship);
    }
}
