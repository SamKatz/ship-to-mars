﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowEveryNSeconds : VisibilityFilter
{
    public float duration = 1;
    public float everyN = 2;


    public override bool ShouldBeVisible()
    {
        return (Time.time % everyN) < duration;
    }
}
