﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Utils {

    public static void EnableAllRenderers(GameObject obj)
    {
        EnableOrDisableAllRenderers(obj, true);
    }

    public static void DisableAllRenderers(GameObject obj)
    {
        EnableOrDisableAllRenderers(obj, false);
    }

    public static void EnableOrDisableAllRenderers(GameObject obj, bool enable)
    {
        List<Renderer> renderers = new List<Renderer>();
        renderers.AddRange(obj.GetComponents<Renderer>());
        renderers.AddRange(obj.GetComponentsInChildren<Renderer>());

        foreach (Renderer r in renderers) r.enabled = enable;
    }

    public static void EnableAllBehaviours<T>(GameObject obj) where T : Behaviour
    {
        EnableOrDisableAllBehaviours<T>(obj, true);
    }

    public static void DisableAllBehaviours<T>(GameObject obj) where T : Behaviour
    {
        EnableOrDisableAllBehaviours<T>(obj, false);
    }

    public static void EnableOrDisableAllBehaviours<T>(GameObject obj, bool enable) where T : Behaviour
    {
        List<T> behaviours = new List<T>();
        behaviours.AddRange(obj.GetComponents<T>());
        behaviours.AddRange(obj.GetComponentsInChildren<T>());

        foreach (T b in behaviours) b.enabled = enable;
    }

    public static void SetAllRenderColors(GameObject obj, Color col)
    {


        if (obj.GetComponent<SpriteRenderer>() != null) SetRenderColor(obj.GetComponent<SpriteRenderer>(), col);
        if (obj.GetComponent<MeshRenderer>() != null) SetRenderColor(obj.GetComponent<MeshRenderer>(), col);

        foreach (SpriteRenderer sr in obj.gameObject.GetComponentsInChildren<SpriteRenderer>())
        {
            SetRenderColor(sr, col);
        }

        foreach (MeshRenderer mr in obj.gameObject.GetComponentsInChildren<MeshRenderer>())
        {
            SetRenderColor(mr, col);
        }
        
    }

    public static void SetRenderColor(Renderer render, Color col)
    {
        
        if (render is SpriteRenderer)
        {
            ((SpriteRenderer)render).color = col;
        }
        else if (render is MeshRenderer)
        {
            ((MeshRenderer)render).material.color = col;
        }

    }

    public static void Restart()
    {
        Game.UpdateStats();
        SceneManager.LoadScene("newmainmenu");
        
    }

    public static string colorToColorTag(Color col)
    {
        return "<color=#" + ColorUtility.ToHtmlStringRGB(col) + ">";
    }

    public static string formatScoreToCash(int score)
    {
        if (score == 0) return "$0";


        return ("$" + score + ",000,000,000").Replace("$-", "-$"); ;
    }




    public static bool animatorHasParameter(Animator anim, string paramName)
    {
        foreach (AnimatorControllerParameter acp in anim.parameters)
        {
            if (acp.name == paramName) return true;
            
        }

        return false;
    }
}
