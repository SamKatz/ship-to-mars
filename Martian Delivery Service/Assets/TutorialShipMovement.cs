﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialShipMovement : MonoBehaviour {
    //This could also happen based on events but 
    public Transform EarthCenter;
    public Transform MarsCenter;

    public Animator myAnimator;
    public float fOrbitSpeed = 10f;
    bool orbitingMars = false;
	// Use this for initialization
	void Start ()
    {
      if(myAnimator == null)
        {
            myAnimator = gameObject.GetComponent<Animator>();
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (EarthCenter != null)
        {
            if(EarthCenter.gameObject.activeInHierarchy == true)
            {
                if (myAnimator.GetBool("OrbitEarth") == false)
                {
                    myAnimator.SetBool("OrbitEarth", true);
                }
            }
        }
        if(orbitingMars == true && MarsCenter != null)
        {
            MarsCenter.Rotate(new Vector3(0, 0, fOrbitSpeed * -1f * Time.deltaTime));
        }
    }

   
    void OrbitMars()
    {
        if (EarthCenter != null && EarthCenter != null)
        {
            if(EarthCenter.parent != MarsCenter)
                EarthCenter.SetParent(MarsCenter, true);
            if (myAnimator.GetBool("OrbitEarth") == true)
            {
                myAnimator.SetBool("OrbitEarth", false);
            }
            orbitingMars = true;
            myAnimator.enabled = false;
        }
    }

    

}
