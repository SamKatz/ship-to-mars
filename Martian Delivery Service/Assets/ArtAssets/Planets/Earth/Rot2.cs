﻿using UnityEngine;
using System.Collections;

public class Rot2 : MonoBehaviour
{

	public float angle = 30;
	public bool reverseDir = false;

    public void Start()
    {
        StartCoroutine("Rotate90");
    }

    public IEnumerator Rotate90()
    {
        Vector3 axis = Vector3.down;

		if (reverseDir)
			axis = Vector3.up;


        float angle = 30.0f;
        float rotAmount = 0.0f;

        Quaternion startingRot = transform.rotation;

        while (rotAmount < angle)
        {
            rotAmount = Mathf.SmoothStep(0.0f, angle, Time.deltaTime);
            transform.RotateAround(axis, rotAmount);
            yield return 0;
        }
    }
}