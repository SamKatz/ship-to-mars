﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TemporaryCurrentTurn : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Game.GetInstance().GetCurrentPlayer() == null)
        {
            this.gameObject.GetComponent<Text>().text = "";
            return;
        }
        this.gameObject.GetComponent<Text>().text = "Player " + (Game.GetInstance().playerNumber+1) + "'s turn.";
        this.gameObject.GetComponent<Text>().color = Game.GetInstance().GetCurrentPlayer().color;
	}
}
