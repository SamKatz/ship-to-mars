﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct HiddenObjects
{
    public float _timeForNextReveal;
    public List<GameObject> ObjectGroup;
    public List<GameObject> HideObject;
}

public class HideObjectsByTime : MonoBehaviour {

    public float TimeForFirstReveal = 1.0f;
    private int hiddenObjectIndex = 0;
    private float _timeBarrier;




    public List<HiddenObjects> HiddenObjects;

	// Use this for initialization
	void Start ()
    {
        _timeBarrier = Time.time + TimeForFirstReveal;
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.Space))
        {
            foreach (GameObject g in HiddenObjects[HiddenObjects.Count - 1].ObjectGroup)
            {
                if (g != null)
                {
                    g.SetActive(true);
                }
            }
        }
        if (Time.time > _timeBarrier && hiddenObjectIndex < HiddenObjects.Count)
        {
            foreach(GameObject g in HiddenObjects[hiddenObjectIndex].ObjectGroup)
            {
                if(g != null)
                {
                    g.SetActive(true);
                }
            }

            foreach (GameObject g in HiddenObjects[hiddenObjectIndex].HideObject)
            {
                if (g != null)
                {
                    g.SetActive(false);
                }
            }
            _timeBarrier = Time.time + HiddenObjects[hiddenObjectIndex]._timeForNextReveal;
            hiddenObjectIndex++;
           
        }
	}
}
