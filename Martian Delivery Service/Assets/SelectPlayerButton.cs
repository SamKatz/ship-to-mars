﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectPlayerButton : MonoBehaviour {

    public Toggle[] toggles;
    public int[] toggleNums;

	public void ButtonPressed()
    {
        for(int i = 0; i < toggles.Length; i++)
        {
            if (!toggles[i].isOn) continue;
            Game.numPlayers = toggleNums[i];
            StartCoroutine(StartNextScene());
        }
    }

    IEnumerator StartNextScene()
    {
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene("mainscene");
    }
}
