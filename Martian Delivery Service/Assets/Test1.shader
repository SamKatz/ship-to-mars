// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:32719,y:32712,varname:node_4013,prsc:2|emission-3047-OUT;n:type:ShaderForge.SFN_Add,id:3047,x:32516,y:32876,varname:node_3047,prsc:2|A-4829-OUT,B-5443-OUT;n:type:ShaderForge.SFN_Multiply,id:4829,x:32243,y:32833,varname:node_4829,prsc:2|A-8348-OUT,B-6793-OUT,C-8095-RGB;n:type:ShaderForge.SFN_Multiply,id:5443,x:32199,y:33079,varname:node_5443,prsc:2|A-2543-RGB,B-6649-OUT;n:type:ShaderForge.SFN_Fresnel,id:6649,x:32030,y:33126,varname:node_6649,prsc:2|EXP-4973-OUT;n:type:ShaderForge.SFN_Color,id:2543,x:31808,y:32979,ptovrint:False,ptlb:node_2543,ptin:_node_2543,varname:node_2543,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.5034485,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:4973,x:31684,y:33235,ptovrint:False,ptlb:Fresnel Expopnent,ptin:_FresnelExpopnent,varname:node_4973,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4072349,max:10;n:type:ShaderForge.SFN_LightColor,id:8095,x:32003,y:32871,varname:node_8095,prsc:2;n:type:ShaderForge.SFN_LightAttenuation,id:6793,x:32003,y:32661,varname:node_6793,prsc:2;n:type:ShaderForge.SFN_Lerp,id:8348,x:31946,y:32543,varname:node_8348,prsc:2|A-1105-RGB,B-9747-RGB,T-3640-OUT;n:type:ShaderForge.SFN_Color,id:1105,x:31589,y:32424,ptovrint:False,ptlb:node_1105,ptin:_node_1105,varname:node_1105,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:9747,x:31589,y:32615,ptovrint:False,ptlb:node_9747,ptin:_node_9747,varname:node_9747,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5294118,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Dot,id:3640,x:31551,y:32828,varname:node_3640,prsc:2,dt:0|A-3587-OUT,B-3365-OUT;n:type:ShaderForge.SFN_LightVector,id:3587,x:31287,y:32694,varname:node_3587,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:3365,x:31259,y:32884,prsc:2,pt:False;n:type:ShaderForge.SFN_Slider,id:5003,x:32150,y:33312,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_5003,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.732618,max:1;proporder:2543-4973-1105-9747-5003;pass:END;sub:END;*/

Shader "Shader Forge/Test1" {
    Properties {
        _node_2543 ("node_2543", Color) = (0,0.5034485,1,1)
        _FresnelExpopnent ("Fresnel Expopnent", Range(0, 10)) = 0.4072349
        _node_1105 ("node_1105", Color) = (0,0,0,1)
        _node_9747 ("node_9747", Color) = (0.5294118,0,0,1)
        _Opacity ("Opacity", Range(0, 1)) = 0.732618
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _node_2543;
            uniform float _FresnelExpopnent;
            uniform float4 _node_1105;
            uniform float4 _node_9747;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float3 emissive = ((lerp(_node_1105.rgb,_node_9747.rgb,dot(lightDirection,i.normalDir))*attenuation*_LightColor0.rgb)+(_node_2543.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelExpopnent)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _node_2543;
            uniform float _FresnelExpopnent;
            uniform float4 _node_1105;
            uniform float4 _node_9747;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 finalColor = 0;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
