﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeTextOnTime : MonoBehaviour {

    public Text animText;
    public float animTime = 1f;
    private char[] animChars;
    private string textString;

    public string EndString ="";

    int animCharIndex = 0;
    private float letterPlaceInterval =.1f;
    private float nextPlacementTime = 0;

	// Use this for initialization
	void Start ()
    {
        if(animText == null)
        {
            animText = gameObject.GetComponent<Text>();
        }
        textString = animText.text;
        animText.text = "";
        animChars = textString.ToCharArray();
        letterPlaceInterval = animTime / animChars.Length;

        nextPlacementTime = Time.time + letterPlaceInterval;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(Time.time > nextPlacementTime && animCharIndex < animChars.Length)
        {
            animText.text += animChars[animCharIndex].ToString();
            animCharIndex++;
            nextPlacementTime = Time.time + letterPlaceInterval;
        }
        else if(animCharIndex >= animChars.Length && EndString != "")
        {
            animText.text = EndString;
        }
    }
}
